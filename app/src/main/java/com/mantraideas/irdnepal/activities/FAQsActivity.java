package com.mantraideas.irdnepal.activities;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.adapters.AdapterAttachment;
import com.mantraideas.irdnepal.helpers.DataQuery;
import com.mantraideas.irdnepal.helpers.DatabaseHelper;
import com.mantraideas.irdnepal.helpers.OnDataReceived;
import com.mantraideas.irdnepal.models.Attachments;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class FAQsActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs_tab);
        setTitle(getResources().getString(R.string.faqs));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_faqs_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment //implements OnDataReceived
    {
        private static final String ARG_SECTION_NUMBER = "section_number";
        private RecyclerView rv_faqs;
        private List<Attachments> attachmentsList;
        private AdapterAttachment mAdapter;
        private DatabaseHelper db;
        //private DataQuery dq;
        private TextView nodata;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(String sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_faqs_tab, container, false);
            return rootView;
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            rv_faqs = (RecyclerView) getView().findViewById(R.id.rv_faqs);
            rv_faqs.setLayoutManager(new LinearLayoutManager(getContext()));
            db = new DatabaseHelper(getContext());
            attachmentsList = db.getAttachments(getArguments().getString(ARG_SECTION_NUMBER), null);
            Utils.log("Tab num "+getArguments().getString(ARG_SECTION_NUMBER));
            mAdapter = new AdapterAttachment(getContext(), attachmentsList);
            rv_faqs.setAdapter(mAdapter);
            nodata = (TextView) getView().findViewById(R.id.no_data);
            if (attachmentsList.size() == 0) {

                if (nodata != null) {
                    nodata.setVisibility(View.VISIBLE);
                }
//                dq = new DataQuery(getContext(), this);
//                dq.getRequestData(Constants.BASE_URL_ATTATCHMENTS);

            } else {
                if (nodata != null) {
                    nodata.setVisibility(View.INVISIBLE);
                }
            }
        }

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private String[] tabs;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs = getResources().getStringArray(R.array.tabs);
        }

        @Override
        public Fragment getItem(int position) {
            String pos = String.valueOf(position+5);
            Utils.log(pos + " position");
            return PlaceholderFragment.newInstance(pos);
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }
    }
}
