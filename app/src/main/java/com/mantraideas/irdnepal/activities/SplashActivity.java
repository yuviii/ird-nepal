package com.mantraideas.irdnepal.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.AlarmManagerUtils;
import com.mantraideas.irdnepal.helpers.DatabaseHelper;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.helpers.SyncService;
import com.mantraideas.irdnepal.models.Alarm;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

public class SplashActivity extends AppCompatActivity {
    DatabaseHelper db;
    Preference pf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        pf = new Preference(this);
        db = new DatabaseHelper(this);
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        Utils.log("Token  = " + refreshedToken);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String data = (String) bundle.get("data");
            Intent intent;
            JSONObject json = new JSONObject();
            Set<String> keys = bundle.keySet();
            for (String key : keys) {
                try {
                    json.put(key, bundle.get(key));

                } catch(JSONException e) {
                    e.printStackTrace();
                }
            }
            pf = new Preference(this);
            pf.setPreference("notification", json.toString());
            Utils.log("JSON = " + json.toString());
            if (data == null || data.length() == 0) {
                intent = new Intent(this, MainActivity.class);
            } else {

                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(data));
            }
            startActivity(intent);
            return;
        }

        boolean aa = pf.getBoolPreference("is_lang_selected", false);
        if (aa) {
            findViewById(R.id.ll_lang).setVisibility(View.GONE);
            syncData();
        } else {
            View.OnClickListener clk = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pf.setBoolPreference("is_lang_selected", true);

                    switch (v.getId()) {
                        case R.id.card_english:
                            pf.setPreference("pref_lang", "English");
                            break;
                        case R.id.card_nepali:
                            pf.setPreference("pref_lang", "Nepali");
                            break;
                    }
                    findViewById(R.id.ll_lang).setVisibility(View.GONE);
                    syncData();

                }
            };
            findViewById(R.id.card_english).setOnClickListener(clk);
            findViewById(R.id.card_nepali).setOnClickListener(clk);
        }
    }

    public void syncData() {
        try {


            JSONArray j = new JSONArray(loadJSONFromAsset());
            if (!db.isDataExists(DBConstants.TABLE_ALARM)) {
                for (int i = 0; i < j.length(); i++) {
                    Alarm a = new Alarm(j.getJSONObject(i));
                    db.addAlarm(a);
                }
            }
            Utils.log(db.getDataInJSON(DBConstants.TABLE_ALARM) + "Alarm Data");
            Utils.log(j.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new AlarmManagerUtils(this).initializedAlarm();
        if(!db.isDataExists(DBConstants.TABLE_ATTACHMENTS) &&!db.isDataExists(DBConstants.TABLE_CONTACT_LIST) ) {
            startService(new Intent(this, SyncService.class));
        }
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();

            }
        }, 3000);

    }

    public String loadJSONFromAsset() {
        String json = "";
        try {
            InputStream is = getApplicationContext().getAssets().open("sample.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return json;
    }


}
