package com.mantraideas.irdnepal.activities.taxpayerportal;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.soap.EstimatedReturns;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.OperationResult;
import com.mantraideas.irdnepal.soap.Payment;
import com.mantraideas.irdnepal.soap.SMSResponse;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.Utils;

public class PaymentInfoActivity extends AppCompatActivity {
    Preference pf;
    ProgressDialog pg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_payment_info);
        initialize();
        pf = new Preference(this);
        pg = new ProgressDialog(this);
        pg.setMessage("Loading...");
        showAlert();
    }

    private void initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.alert_payment_info, null, false);
        final EditText voucher = (EditText) view.findViewById(R.id.voucher);
        view.setPadding(16,16,16,16);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Utils.log("Voucher No. = "+ voucher.getText().toString());
                callServer(voucher.getText().toString());
            }
        }).setTitle("Please enter voucher number").setView(view).setCancelable(false).show();
    }

    private void callServer(String voucher) {
        IIntegratedTaxSystemService service = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                pg.show();
            }

            @Override
            public void Completed(OperationResult result) {
                Utils.log("Payment="+result.Result);
             try {
                 SMSResponse response = (SMSResponse) result.Result;
                 Payment data = response.Payment;
                 ((TextView) findViewById(R.id.txt_date)).setText(data.Date==null||data.Date.isEmpty()?"N/A":data.Date);
                 ((TextView) findViewById(R.id.txt_payment_type)).setText(data.PaymentType==null||data.PaymentType.isEmpty()?"N/A":data.PaymentType);
                 ((TextView) findViewById(R.id.txt_amount)).setText(data.Amount==null||data.Amount.isEmpty()?"N/A":data.Amount);
                 ((TextView) findViewById(R.id.txt_paymentfor)).setText(data.PaymentFor==null||data.PaymentFor.isEmpty()?"N/A":data.PaymentFor);

             }catch (Exception e){
                 Snackbar.make(findViewById(R.id.txt_date),"No data available",Snackbar.LENGTH_INDEFINITE).show();
             }
                   pg.dismiss();


            }
        });
        service.GetPaymentInfoForMobileAsync(Constants.IRD_CLIENT_ID, Constants.IRD_TOKEN, pf.getPreference("pan"), voucher);
    }

}
