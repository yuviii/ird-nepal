package com.mantraideas.irdnepal.activities.taxpayerportal;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.OperationResult;
import com.mantraideas.irdnepal.soap.SMSResponse;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.Utils;

public class VATReturnsInfoActivity extends AppCompatActivity {
    Preference pf;
    ProgressDialog pg;
    TextView message,pan,trade_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vatreturns_info);
        initialize();
        pf  = new Preference(this);
        pg = new ProgressDialog(this);
        pg.setMessage("Loading VAT Returns Info ...");
        showAlert();
    }
    private void initialize() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        message = (TextView) findViewById(R.id.message);
        pan = (TextView) findViewById(R.id.pan);
        trade_name = (TextView) findViewById(R.id.trade_name);
    }
    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.alert_spinner1, null, false);
        final Spinner filingperiod = (Spinner) view.findViewById(R.id.filing_period);
        final Spinner period = (Spinner) view.findViewById(R.id.period);
        final EditText year = (EditText) view.findViewById(R.id.year);
        ArrayAdapter<CharSequence> adapter_type = ArrayAdapter.createFromResource(this,
                R.array.filing_period, android.R.layout.simple_spinner_item);
        adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        filingperiod.setAdapter(adapter_type);

        ArrayAdapter<CharSequence> adapter_period = ArrayAdapter.createFromResource(this,
                R.array.period, android.R.layout.simple_spinner_item);
        adapter_period.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        period.setAdapter(adapter_period);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callServer(year.getText().toString(), filingperiod.getSelectedItem().toString(), period.getSelectedItem().toString());
            }
        }).setTitle("Please enter details:").setView(view).setCancelable(false).show();
    }

    private void callServer(final String year, String fp, String period) {
        IIntegratedTaxSystemService service = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                pg.show();
                Utils.log("Starting service: get Vat returns");
            }

            @Override
            public void Completed(OperationResult result) {
                pg.hide();
                if(result==null){
                    Snackbar.make(findViewById(R.id.fab),"Connection error",Snackbar.LENGTH_INDEFINITE).show();
                    return;
                }
                try {
                    SMSResponse response = (SMSResponse) result.Result;
                    String er = response.Message;
                    pan.setText("PAN : " + pf.getPreference("pan"));
                    trade_name.setText("Trade Name : " + pf.getPreference("trade_name"));
                    message.setText(response.Message);
                }catch (Exception e){
                    Snackbar.make(findViewById(R.id.fab),"No data available",Snackbar.LENGTH_INDEFINITE).show();

                }
                }
        });
        service.GetVATReturnsInfoForMobileAsync(Constants.IRD_CLIENT_ID, Constants.IRD_TOKEN, pf.getPreference("pan"), year, fp, period);
    }


}
