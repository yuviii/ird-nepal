package com.mantraideas.irdnepal.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ContactDetailActivity extends AppCompatActivity {
    TextView name, address, email, fax, viewmap, number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(getResources().getString(R.string.contacts));
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        name = (TextView) findViewById(R.id.dept_name);
        address = (TextView) findViewById(R.id.dept_address);
        number = (TextView) findViewById(R.id.dept_number);
        email = (TextView) findViewById(R.id.dept_email);
        fax = (TextView) findViewById(R.id.dept_fax);
        String data = getIntent().getStringExtra("data");
        try {
            final JSONObject jsonObject = new JSONObject(data);
            name.setText(jsonObject.optString("Name"));
            setTitle("");
            address.setText(jsonObject.optString("Address").equals("null") ||jsonObject.optString("Address").isEmpty()? "N/A" : jsonObject.optString("Address"));
            number.setText(jsonObject.optString("PhoneNumber").equals("null") ||jsonObject.optString("PhoneNumber").isEmpty()? "N/A" : jsonObject.optString("PhoneNumber"));
//            number.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+jsonObject.optString("PhoneNumber"))).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }
//            });
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                boolean isShow = false;
                int scrollRange = -1;

                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.getTotalScrollRange();
                    }
                    if (scrollRange + verticalOffset == 0) {
                        collapsingToolbarLayout.setTitle(jsonObject.optString("Name"));
                        isShow = true;
                    } else if (isShow) {
                        collapsingToolbarLayout.setTitle("");
                        isShow = false;
                    }
                }
            });
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(view.getContext(), MapsActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("lat", jsonObject.optString(DBConstants.COLUMN_LATITUDE));
                    i.putExtra("long", jsonObject.optString(DBConstants.COLUMN_LONGITUDE));
                    i.putExtra("name", jsonObject.optString(DBConstants.COLUMN_NAME));
                    startActivity(i);
                }
            });
            email.setText(jsonObject.optString("Email").equals("null")||jsonObject.optString("Email").isEmpty() ? "N/A" : jsonObject.optString("Email"));
            email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + jsonObject.optString("Email")));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Send Mail");
                    startActivity(Intent.createChooser(emailIntent, getResources().getString(R.string.app_name)));

                }
            });
            fax.setText(jsonObject.optString("Fax").equals("null")||jsonObject.optString("Fax").isEmpty() ? "N/A" : jsonObject.optString("Fax"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utils.log(data);
    }
}
