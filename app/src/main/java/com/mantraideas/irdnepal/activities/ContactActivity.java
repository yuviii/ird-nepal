package com.mantraideas.irdnepal.activities;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.firebase.database.GenericTypeIndicator;
import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.adapters.AdapterContactGroups;
import com.mantraideas.irdnepal.helpers.DataQuery;
import com.mantraideas.irdnepal.helpers.DatabaseHelper;
import com.mantraideas.irdnepal.helpers.OnDataReceived;
import com.mantraideas.irdnepal.models.Attachments;
import com.mantraideas.irdnepal.models.ContactList;
import com.mantraideas.irdnepal.models.Generic;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ContactActivity extends AppCompatActivity //implements OnDataReceived
{
    RecyclerView rv_contact_list;
    AdapterContactGroups mAdapter;
    List<ContactList> contactLists;
//    DataQuery dq;
    DatabaseHelper db;
    List<Generic> listGeneric;
    String header[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(getResources().getString(R.string.contacts));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rv_contact_list = (RecyclerView) findViewById(R.id.rv_contact_list);
        rv_contact_list.setLayoutManager(new LinearLayoutManager(this));
        contactLists = new ArrayList<>();
        db = new DatabaseHelper(this);
        header = getResources().getStringArray(R.array.header_contact_list);
        JSONArray jsonArray = db.getDataInJSON(DBConstants.TABLE_CONTACT_LIST);
        this.listGeneric = getGenericList(jsonArray);
        Utils.log("List Size" + jsonArray.length());
//        if (jsonArray.length() <= 0) {
//            dq = new DataQuery(this, this);
//            dq.getRequestData(Constants.BASE_URL_CONTACTLIST);
//        }
        mAdapter = new AdapterContactGroups(this, this.listGeneric);
        rv_contact_list.setAdapter(mAdapter);

    }

    public List<Generic> getGenericList(JSONArray jsonArray) {
        List<Generic> listGeneric = new ArrayList<>();
        for (int i = 0; i < getResources().getStringArray(R.array.header_contact_list).length; i++) {
            Generic g = new Generic(header[i], 0);
            Utils.log("Header " + g.data + " " + g.type);
            listGeneric.add(g);
            for (int j = 0; j < jsonArray.length(); j++) {
                try {
                    JSONObject ja = jsonArray.getJSONObject(j);
                    Utils.log("aalu " + ja.toString());
                    if (ja.optString(DBConstants.COLUMN_CONTACT_GROUP_ID).equals(i + 1 + "")) {
                        Utils.log(i + "=id");
                        Generic s = new Generic(ja.toString(), 1);
                        Utils.log("List item " + ja.toString() + "type:" + s.type);
                        listGeneric.add(s);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return listGeneric;
    }
//
//    @Override
//    public void onSuccess(String table_name, String result) {
//        if (!result.equals("")) {
//            try {
//                JSONArray jsonArray = new JSONArray(result);
//                for (int i = 0; i < jsonArray.length(); i++) {
//                    JSONObject object = jsonArray.getJSONObject(i);
//                    if (!db.isContactListExists(DBConstants.TABLE_CONTACT_LIST, object.optString(DBConstants.COLUMN_CONTACT_LIST_ID))) {
//                        db.addContactList(object);
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        JSONArray jsonArray = db.getDataInJSON(DBConstants.TABLE_CONTACT_LIST);
//        this.listGeneric = getGenericList(jsonArray);
//        Utils.log("List Size(On Success)" + jsonArray.length());
//        mAdapter.notifyDataSetChanged();
//    }
//
//    @Override
//    public void onError(String table_name, int status, String message) {
//
//    }
}
