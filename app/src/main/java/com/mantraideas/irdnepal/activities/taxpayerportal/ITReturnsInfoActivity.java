package com.mantraideas.irdnepal.activities.taxpayerportal;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.soap.EstimatedReturns;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.OperationResult;
import com.mantraideas.irdnepal.soap.SMSResponse;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.Utils;

public class ITReturnsInfoActivity extends AppCompatActivity {

    Preference pf ;
    ProgressDialog pg;
    TextView message,pan,trade_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_it_returns_info);
        initialize();
        pf = new Preference(this);
        pg = new ProgressDialog(this);
        pg.setMessage("Loading...");
        showAlert();
    }
    private void initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        message = (TextView) findViewById(R.id.message);
        pan = (TextView) findViewById(R.id.pan);
        trade_name = (TextView) findViewById(R.id.trade_name);
    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.alert_spinner, null, false);
        final Spinner spn = (Spinner) view.findViewById(R.id.fiscal_year);
        ArrayAdapter<CharSequence> adapter_type = ArrayAdapter.createFromResource(this,
                R.array.fiscal_year, android.R.layout.simple_spinner_item);
        adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn.setAdapter(adapter_type);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callServer(spn.getSelectedItem().toString());
            }
        }).setTitle("Please select fiscal year").setView(view).show();

    }


    private void callServer(String fy) {
        IIntegratedTaxSystemService service = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                Utils.log("Starting setrice: get estimated returns");
                pg.show();
            }

            @Override
            public void Completed(OperationResult result) {
                if(result==null){
                    Snackbar.make(findViewById(R.id.fab),"Connection error",Snackbar.LENGTH_INDEFINITE).show();
                    return;
                }
                SMSResponse response = (SMSResponse) result.Result;
                if(response.Message!=null){
                    pan.setText("PAN : "+pf.getPreference("pan"));
                    trade_name.setText("Trade Name : "+pf.getPreference("trade_name"));
                    message.setText(response.Message);
//                    Snackbar.make(findViewById(R.id.fab),response.Message,Snackbar.LENGTH_INDEFINITE).show();
                }else {
                    Snackbar.make(findViewById(R.id.fab),"No Data Available",Snackbar.LENGTH_INDEFINITE).show();
                }
                pg.dismiss();

            }
        });
        service.GetITReturnsInfoForMobileAsync(Constants.IRD_CLIENT_ID,Constants.IRD_TOKEN,pf.getPreference("pan"),fy);
    }


}
