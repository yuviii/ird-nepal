package com.mantraideas.irdnepal.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.activities.AttachmentListActivity;
import com.mantraideas.irdnepal.helpers.OnListItemClickListener;
import com.mantraideas.irdnepal.utilities.Constants;

public class TaxLawsActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tax_laws);
        setTitle(getResources().getString(R.string.tax_laws));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tax_laws, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private ListView rv_taxlaws;
        private static final String ARG_SECTION_NUMBER = "section_number";
        ArrayAdapter<String> adapter;
        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tax_laws, container, false);
            rv_taxlaws = (ListView) rootView.findViewById(R.id.rv_taxlaws);
            final String[] ACTS = getResources().getStringArray(R.array.Acts);
            final String[] DIRECTIVES = getResources().getStringArray(R.array.Directives);
            final String[] REGULATIONS = getResources().getStringArray(R.array.Regulations);
            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1:
                    adapter = new ArrayAdapter<String>(getContext(),
                            R.layout.row_text, ACTS){
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            if(convertView == null)
                            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_text, null);
                            ((TextView) convertView.findViewById(R.id.text)).setText((position+1) + ". " + getItem(position));
                            return convertView;
                        }
                    };
                    break;
                case 2:
                    adapter = new ArrayAdapter<String>(getContext(),
                            R.layout.row_text, R.id.text, DIRECTIVES){
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            if(convertView == null)
                                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_text, null);
                            ((TextView) convertView.findViewById(R.id.text)).setText((position+1) + ". " + getItem(position));
                            return convertView;
                        }
                    };
                    break;
                case 3:
                    adapter = new ArrayAdapter<String>(getContext(),
                            R.layout.row_text, R.id.text, REGULATIONS){
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            if(convertView == null)
                                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_text, null);
                            ((TextView) convertView.findViewById(R.id.text)).setText((position+1) + ". " + getItem(position));
                            return convertView;
                        }
                    };
                    break;
                default:


            }
            rv_taxlaws.setAdapter(adapter);
            rv_taxlaws.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(view.getContext(), AttachmentListActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("contentId",Constants.CONTENT_ID[getArguments().getInt(ARG_SECTION_NUMBER)-1][position]);
                    switch (getArguments().getInt(ARG_SECTION_NUMBER)){
                        case 1:
                            i.putExtra("title",ACTS[position]);
                            break;
                        case 2:
                            i.putExtra("title",DIRECTIVES[position]);
                            break;
                        case 3:
                            i.putExtra("title",REGULATIONS[position]);
                            break;
                    }
                    startActivity(i);

                }
            });
            // TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        String[] tabs;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs = getResources().getStringArray(R.array.tabs_tax_laws);
        }
        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return tabs[position];
        }
    }
}
