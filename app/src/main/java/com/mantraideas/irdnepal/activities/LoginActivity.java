package com.mantraideas.irdnepal.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.LoginResponse;
import com.mantraideas.irdnepal.soap.OperationResult;
import com.mantraideas.irdnepal.soap.PANSearchDetails;
import com.mantraideas.irdnepal.soap.SMSResponse;
import com.mantraideas.irdnepal.soap.TPRegistration;
import com.mantraideas.irdnepal.soap.TaxPayerLogin;
import com.mantraideas.irdnepal.soap.TaxpayerLoginResponse;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.Utils;

import okhttp3.internal.Util;

public class LoginActivity extends AppCompatActivity {

    EditText edt_pan, edt_user_name, edt_password;
    Button login_btn;
    String pan;
    ProgressDialog progressDialog;
    Preference pf;
    int loginType;
    LinearLayout ll_pan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(this);
        edt_pan = (EditText) findViewById(R.id.pan_number);
        ll_pan = (LinearLayout) findViewById(R.id.ll_pan);
        edt_user_name = (EditText) findViewById(R.id.userName);
        pf = new Preference(this);
        edt_password = (EditText) findViewById(R.id.password);
        login_btn = (Button) findViewById(R.id.login_btn);
        showAlert();

    }

    private void officeLogin() {
        ll_pan.setVisibility(View.GONE);
        final IIntegratedTaxSystemService service_tds = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                Utils.log("Starting service");
            }

            @Override
            public void Completed(OperationResult result) {
                try {
                    LoginResponse r = (LoginResponse) result.Result;
                    if (r.User.LoggedIn) {
                        pf.setBoolPreference("isLoggedIn", true);
                        progressDialog.dismiss();
                        pf.setPreference("pan", "");
                        pf.setBoolPreference(Constants.LOGIN_OFFICE, true);
                        pf.setBoolPreference(Constants.LOGIN_TAXPAYER, false);
                        pf.setBoolPreference(Constants.LOGIN_TDS, false);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    } else {
                        progressDialog.dismiss();
                        Snackbar.make(findViewById(R.id.login_btn), r.Message, Snackbar.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Snackbar.make(findViewById(R.id.login_btn), "Connection error.", Snackbar.LENGTH_LONG).show();
                }
            }


        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Loging in ....");
                progressDialog.setTitle("Please Wait");
                progressDialog.show();
                pan = edt_pan.getText().toString();
                service_tds.ValidateOfficerLoginAsync(Constants.IRD_CLIENT_ID, Constants.IRD_TOKEN, edt_user_name.getText().toString(), edt_password.getText().toString());
            }
        });


    }

    private void tdsLogin() {
        final IIntegratedTaxSystemService service_tds = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                Utils.log("Starting service");
            }

            @Override
            public void Completed(OperationResult result) {
                try {
                    TaxpayerLoginResponse r = (TaxpayerLoginResponse) result.Result;
                    if (r.IsValid) {
                        pf.setPreference("pan", pan);
                        pf.setBoolPreference("isLoggedIn", true);
                        progressDialog.dismiss();
                        pf.setBoolPreference(Constants.LOGIN_TDS, true);
                        pf.setBoolPreference(Constants.LOGIN_OFFICE, false);
                        pf.setBoolPreference(Constants.LOGIN_TAXPAYER, false);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    } else {
                        progressDialog.dismiss();
                        Snackbar.make(findViewById(R.id.login_btn), r.Message, Snackbar.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Snackbar.make(findViewById(R.id.login_btn),"Connection error", Snackbar.LENGTH_LONG).show();

                }
            }


        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Loging in ....");
                progressDialog.setTitle("Please Wait");
                progressDialog.show();
                pan = edt_pan.getText().toString();
                service_tds.ValidateTDSTaxpayerLoginAsync(Constants.IRD_CLIENT_ID, Constants.IRD_TOKEN, edt_pan.getText().toString(), edt_user_name.getText().toString(), edt_password.getText().toString());
            }
        });


    }

    private void taxPayerLogin() {
        final IIntegratedTaxSystemService service = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                Utils.log("Starting service");
            }

            @Override
            public void Completed(OperationResult result) {
                progressDialog.dismiss();
                try {
                    TaxpayerLoginResponse r = (TaxpayerLoginResponse) result.Result;
                    if (r.IsValid) {
                        pf.setPreference("pan", pan);
                        pf.setBoolPreference("isLoggedIn", true);
                        progressDialog.dismiss();
                        pf.setBoolPreference(Constants.LOGIN_TAXPAYER, true);
                        pf.setBoolPreference(Constants.LOGIN_TDS, false);
                        pf.setBoolPreference(Constants.LOGIN_OFFICE, false);
                        startActivity(new Intent(getApplicationContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    } else {
                        Snackbar.make(findViewById(R.id.login_btn), r.Message, Snackbar.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Snackbar.make(findViewById(R.id.login_btn), "Conetion error.", Snackbar.LENGTH_LONG).show();
                }
            }


        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Loging in ....");
                progressDialog.setTitle("Please Wait");
                progressDialog.show();
                pan = edt_pan.getText().toString();
                service.ValidateTaxpayerLoginAsync(Constants.IRD_CLIENT_ID, Constants.IRD_TOKEN, edt_pan.getText().toString(), edt_user_name.getText().toString(), edt_password.getText().toString());
            }
        });


    }

    public void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final Spinner spn = new Spinner(this);
        spn.setPadding(16,16,16,16);
        ArrayAdapter<CharSequence> adapter_type = ArrayAdapter.createFromResource(this,
                R.array.login_type, android.R.layout.simple_spinner_item);
        adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn.setAdapter(adapter_type);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                loginType = spn.getSelectedItemPosition();
                switch (loginType) {
                    case 0:
                        Utils.log("Login type:" + loginType);
                        taxPayerLogin();
                        break;
                    case 1:
                        Utils.log("Login type = " + loginType);
                        tdsLogin();
                        break;
                    case 2:
                        officeLogin();
                        break;


                }
                setTitle(spn.getSelectedItem().toString());
            }
        }).setTitle("Please select login type").setCancelable(false).setView(spn).show();
    }

}
