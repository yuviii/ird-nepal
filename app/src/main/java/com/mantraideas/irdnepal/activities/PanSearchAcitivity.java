package com.mantraideas.irdnepal.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.OperationResult;
import com.mantraideas.irdnepal.soap.PANSearchDetails;
import com.mantraideas.irdnepal.soap.PANSearchResponse;
import com.mantraideas.irdnepal.soap.WSHttpBinding_IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.utilities.Utils;


public class PanSearchAcitivity extends AppCompatActivity {

    public static final String TOKEN = "!2D2Sqr&c3VOR@TMxMDAzMTA3MDAwTOK3PO===";
    public static final String CLIENT_ID = "M0b!leAppT0!RD";
    public String PAN = "601117352";
    private LinearLayout ll_content;
    private ProgressDialog pd;
    private TextView pan, tradeName, cityName, streetName, ward, telephone, panRegDate, vatRegDate, vatFillingPeriod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pan_search_acitivity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        PAN = getIntent().getStringExtra("pan");
        pan = (TextView) findViewById(R.id.txt_pan);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        tradeName = (TextView) findViewById(R.id.txt_trade_name);
        cityName = (TextView) findViewById(R.id.txt_city_name);
        streetName = (TextView) findViewById(R.id.txt_street_name);
        ward = (TextView) findViewById(R.id.txt_ward);
        telephone = (TextView) findViewById(R.id.txt_telephone);
        panRegDate = (TextView) findViewById(R.id.txt_pan_registration_date);
        vatRegDate = (TextView) findViewById(R.id.txt_vat_registration_date);
        vatFillingPeriod = (TextView) findViewById(R.id.txt_vat_filling);
        IIntegratedTaxSystemService client = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                Utils.log("Starting");
                pd.show();
            }

            @Override
            public void Completed(OperationResult result) {
                pd.hide();
                try {
                    PANSearchResponse rsl = (PANSearchResponse) result.Result;
                    PANSearchDetails detail = rsl.PANDetail;
                    populate(detail);
                } catch (Exception e) {
                    populate(null);
                    e.printStackTrace();

                }
            }
        });
        if (PAN != null) {

            client.SearchPANDetailsAsync(CLIENT_ID, TOKEN, PAN);
        }
    }
    private void populate(PANSearchDetails detail) {
        if(detail!=null){
            pan.setText(detail.PAN);
            tradeName.setText(detail.TradeName);
            cityName.setText(detail.CityName);
            streetName.setText(detail.StreetName);
            ward.setText(detail.Ward);
            telephone.setText(detail.Telephone);
            panRegDate.setText(detail.PanRegistrationDate);
            vatRegDate.setText(detail.VatRegistrationDate);
            vatFillingPeriod.setText(detail.FilingPeriod);
        }else {
            new AlertDialog.Builder(this).setTitle("Record Not Found").setPositiveButton("Back to home", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }).show();
        }

    }


}
