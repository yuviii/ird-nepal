package com.mantraideas.irdnepal.activities;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.adapters.AdapterAttachment;
import com.mantraideas.irdnepal.helpers.DataQuery;
import com.mantraideas.irdnepal.helpers.DatabaseHelper;
import com.mantraideas.irdnepal.helpers.OnDataReceived;
import com.mantraideas.irdnepal.models.Attachments;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PublicationsActivity extends AppCompatActivity implements OnDataReceived {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publications);
        setTitle(getResources().getString(R.string.publications));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_publications, menu);
        return true;
    }

    @Override
    public void onSuccess(String table_name, String result) {

    }

    @Override
    public void onError(String table_name, int status, String message) {

    }

    public static class PlaceholderFragment extends Fragment //implements OnDataReceived
    {
        private static final String ARG_SECTION_NUMBER = "section_number";
        private RecyclerView rv_publications;
//        private DataQuery dq;
        private DatabaseHelper db;
        private List<Attachments> attachmentsList;
        private String contentid;
        private AdapterAttachment mAdapter;
        private TextView no_data;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(String sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_publications, container, false);
            contentid = getArguments().getString(ARG_SECTION_NUMBER);
            return rootView;
        }

//        void request() {
//            dq = new DataQuery(getContext(), this);
//            dq.getRequestData(Constants.BASE_URL_ATTATCHMENTS);
//        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            rv_publications = (RecyclerView) getView().findViewById(R.id.rv_publications);
            rv_publications.setLayoutManager(new LinearLayoutManager(getContext()));
            attachmentsList = new ArrayList<>();
            db = new DatabaseHelper(getContext());
            attachmentsList = db.getAttachments(contentid, null);
            mAdapter = new AdapterAttachment(getContext(), attachmentsList);
            rv_publications.setAdapter(mAdapter);
            no_data = (TextView) getView().findViewById(R.id.no_data);
            if (attachmentsList.size() == 0) {
                if (no_data != null) {
                    no_data.setVisibility(View.VISIBLE);
                }
//                request();

            }
        }
//
//        @Override
//        public void onSuccess(String table_name, String result) {
//                try {
//                    JSONArray jsonArray = new JSONArray(result);
//                    if(jsonArray.length()>0){
//                        if (no_data != null) {
//                            no_data.setVisibility(View.INVISIBLE);
//                        }
//                    }else {
//
//                        if (no_data != null) {
//                            no_data.setVisibility(View.VISIBLE);
//                        }
//                    }
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject object = jsonArray.getJSONObject(i);
//                        if (!db.isExists(DBConstants.TABLE_ATTACHMENTS, object.optString(DBConstants.COLUMN_ATTACHMENT_ID))) {
//                            db.addAttachment(object);
//                        }
//                        if (Utils.contains(object, DBConstants.COLUMN_CONTENT_ID)) {
//                            if (object.optString(DBConstants.COLUMN_CONTENT_ID).equals(contentid)) {
//                                Attachments a = new Attachments(object);
//
//                                this.attachmentsList.add(a);
//                                Utils.log("match :" + object.toString());
//                            }
//
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                mAdapter.notifyDataSetChanged();
//
//        }
//
//        @Override
//        public void onError(String table_name, int status, String message) {
//            no_data.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        String tabs[];

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs = getResources().getStringArray(R.array.tabs_publication);
        }

        @Override
        public Fragment getItem(int position) {
            String id;
            return PlaceholderFragment.newInstance(Constants.ID_PUBLICATIONS[position]);
        }

        @Override
        public int getCount() {
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }
    }
}
