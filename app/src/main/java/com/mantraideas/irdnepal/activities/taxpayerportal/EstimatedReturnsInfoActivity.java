package com.mantraideas.irdnepal.activities.taxpayerportal;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.soap.ArrayOfEstimatedReturns;
import com.mantraideas.irdnepal.soap.EstimatedReturns;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.OperationResult;
import com.mantraideas.irdnepal.soap.SMSResponse;
import com.mantraideas.irdnepal.soap.WithholdeeTransaction;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.Utils;

public class EstimatedReturnsInfoActivity extends AppCompatActivity {
    Preference pf;
    ProgressDialog pg;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estimated_returns_info);
        initialize();
        pg = new ProgressDialog(this);
        pg.setMessage("Loading ... ");
        pf = new Preference(this);
        showAlert();

    }

    private void initialize() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        list = (ListView) findViewById(R.id.list);
    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.alert_spinner, null, false);
        final Spinner spn = (Spinner) view.findViewById(R.id.fiscal_year);
        ArrayAdapter<CharSequence> adapter_type = ArrayAdapter.createFromResource(this,
                R.array.fiscal_year, android.R.layout.simple_spinner_item);
        adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn.setAdapter(adapter_type);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Utils.log("Date = " + spn.getSelectedItem().toString());
                dialog.dismiss();
                callServer(spn.getSelectedItem().toString());
            }
        }).setTitle("Please select fiscal year").setView(view).setCancelable(false).show();
    }

    private void callServer(String fy) {
        IIntegratedTaxSystemService service = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                pg.show();
            }

            @Override
            public void Completed(OperationResult result) {
                if (result == null) {
                    new AlertDialog.Builder(EstimatedReturnsInfoActivity.this).setMessage("Connectionn error").show();
                    return;
                }
                SMSResponse response = (SMSResponse) result.Result;
                pg.dismiss();
                try {
                    ArrayOfEstimatedReturns rt = response.EstimatedReturns;
                    Utils.log("Estimated returns array size: "+rt.size()+"");
                    if (rt.size() <= 0) {
                        findViewById(R.id.nodata).setVisibility(View.VISIBLE);

                    } else {
                        findViewById(R.id.nodata).setVisibility(View.GONE);
                    }
                    ArrayAdapter<EstimatedReturns> aa = new ArrayAdapter<EstimatedReturns>(EstimatedReturnsInfoActivity.this, R.layout.simple_list, rt) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View view = LayoutInflater.from(EstimatedReturnsInfoActivity.this).inflate(R.layout.row_estimated_returns, null);
                            ((TextView) view.findViewById(R.id.txt_date)).setText(getItem(position).FilingDate);
                            ((TextView) view.findViewById(R.id.txt_return_type)).setText(getItem(position).EstimatedType);
                            ((TextView) view.findViewById(R.id.txt_status)).setText(getItem(position).EstimatedIncome);
                            ((TextView)view.findViewById(R.id.fiscal_year)).setText(getItem(position).FiscalYear);
                            ((TextView) view.findViewById(R.id.txt_submission_no)).setText(getItem(position).SubmissionNo);
                            return view;
                        }
                    };
                    list.setAdapter(aa);
                } catch (Exception e) {
                    findViewById(R.id.nodata).setVisibility(View.VISIBLE);
                    Snackbar.make(findViewById(R.id.fab), "No data available", Snackbar.LENGTH_INDEFINITE).show();
                }
            }
        });
        service.GetEstimatedReturnsInfoForMobileAsync(Constants.IRD_CLIENT_ID, Constants.IRD_TOKEN, pf.getPreference("pan"), fy);
    }

}
