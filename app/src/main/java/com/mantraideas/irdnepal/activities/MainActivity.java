package com.mantraideas.irdnepal.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.activities.taxpayerportal.ArrearActivity;
import com.mantraideas.irdnepal.activities.taxpayerportal.CollectionActivity;
import com.mantraideas.irdnepal.activities.taxpayerportal.EstimatedReturnsInfoActivity;
import com.mantraideas.irdnepal.activities.taxpayerportal.ExciseReturnsInfoActivity;
import com.mantraideas.irdnepal.activities.taxpayerportal.ITReturnsInfoActivity;
import com.mantraideas.irdnepal.activities.taxpayerportal.PaymentInfoActivity;
import com.mantraideas.irdnepal.activities.taxpayerportal.VATReturnsInfoActivity;
import com.mantraideas.irdnepal.activities.taxpayerportal.WithHoldeeTransactionActivity;
import com.mantraideas.irdnepal.helpers.DataQuery;
import com.mantraideas.irdnepal.helpers.MyDownloader;
import com.mantraideas.irdnepal.helpers.OnDataReceived;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.helpers.SchedulingService;
import com.mantraideas.irdnepal.models.Notice;
import com.mantraideas.irdnepal.soap.Collection;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.OperationResult;
import com.mantraideas.irdnepal.soap.PANSearchDetails;
import com.mantraideas.irdnepal.soap.PANSearchResponse;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnDataReceived {
    private static final int REQUEST_WRITE_STORAGE = 112;
    DataQuery dq;
    List<Notice> noticeList;
    LinearLayout ll_notice, ll_notification;
    TextView seeAllUpdates, notification, pan, tradeName;
    EditText txt_pan;
    Button login_btn;

    ImageView close_button;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    Preference pf;
    NavigationView navigationView;
    Boolean isLoggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermissionGranted();
        pf = new Preference(this);


        setLanguage();
        setContentView(R.layout.activity_main);
        setTitle(getResources().getString(R.string.app_name));
        setUpDrawer();
        //    initializeLoginItems();
        initializeNotificationBar();
        initializeViews();
        noticeList = new ArrayList<>();
        if (!pf.getPreference("result_notice").isEmpty()) {
            List<Notice> list = Notice.getNoticeList(pf.getPreference("result_notice"));
            noticeList.addAll(list);
        }
        populate();
        if (Utils.isNetworkConnected(this)) {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {
                    dq = new DataQuery(MainActivity.this, MainActivity.this);
                    dq.getRequestData(Constants.BASE_URL_NOTICE);

                    return null;
                }
            }.execute();
        } else {
            onSuccess("", pf.getPreference("result_notice"));
        }

        startService(new Intent(this, SchedulingService.class));
    }
//
//    private void initializeLoginItems() {
//        if (pf.getBoolPreference("isLoggedIn")) {
//
//            if (pf.getBoolPreference(Constants.LOGIN_TAXPAYER)&&findViewById(R.id.row_tax_payer_portal) != null) {
//                findViewById(R.id.row_tax_payer_portal).setVisibility(View.VISIBLE);
//            }
//            if (pf.getBoolPreference(Constants.LOGIN_TDS)&&findViewById(R.id.row_tax_payer_portal) != null) {
//                findViewById(R.id.row_tds_portal).setVisibility(View.VISIBLE);
//            }
//            login_btn.setVisibility(View.GONE);
//            pan.setText(pf.getPreference("pan"));
//            if (pf.getPreference("trade_name").equals("")) {
//                getRegistrationDetail(pf.getPreference("pan"));
//            } else {
//                tradeName.setText(pf.getPreference("trade_name"));
//            }
//        } else {
//            if (findViewById(R.id.row_tax_payer_portal) != null) {
//                findViewById(R.id.row_tax_payer_portal).setVisibility(View.GONE);
//            }
//            if (findViewById(R.id.row_tds_portal) != null) {
//                findViewById(R.id.row_tds_portal).setVisibility(View.GONE);
//            }
//        }
//
//        handleRequests();
//    }

    private void handleRequests() {

        View.OnClickListener clicklistener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.card_it_returns:
                        startActivity(new Intent(MainActivity.this, ITReturnsInfoActivity.class));
                        break;
                    case R.id.card_excise:
                        startActivity(new Intent(MainActivity.this, ExciseReturnsInfoActivity.class));
                        break;
                    case R.id.card_estimated:
                        startActivity(new Intent(MainActivity.this, EstimatedReturnsInfoActivity.class));
                        break;
                    case R.id.card_payment:
                        startActivity(new Intent(MainActivity.this, PaymentInfoActivity.class));
                        break;
                    case R.id.card_vat_returns:
                        startActivity(new Intent(MainActivity.this, VATReturnsInfoActivity.class));
                        break;
                    case R.id.card_arrear:
                        startActivity(new Intent(MainActivity.this, ArrearActivity.class));
                        break;
//                    case R.id.ll_collection:
//                        startActivity(new Intent(MainActivity.this, CollectionActivity.class));
//                        break;
                }
            }
        };
        findViewById(R.id.card_it_returns).setOnClickListener(clicklistener);
        findViewById(R.id.card_excise).setOnClickListener(clicklistener);
        findViewById(R.id.card_estimated).setOnClickListener(clicklistener);
        findViewById(R.id.card_payment).setOnClickListener(clicklistener);
        findViewById(R.id.card_arrear).setOnClickListener(clicklistener);
        findViewById(R.id.card_vat_returns).setOnClickListener(clicklistener);
//        findViewById(R.id.ll_collection).setOnClickListener(clicklistener);

    }
     

    private void getRegistrationDetail(final String pan) {
        try {
            IIntegratedTaxSystemService serv = new IIntegratedTaxSystemService(new IServiceEvents() {
                @Override
                public void Starting() {
                    Utils.log("Starting Service Function : getRestrationDetail");
                }

                @Override
                public void Completed(OperationResult result) {
                    try {
                        PANSearchResponse response = (PANSearchResponse) result.Result;
                        PANSearchDetails detail = response.PANDetail;
                        if (detail.TradeName != null) {
                            pf.setPreference("trade_name", detail.TradeName);
                            MainActivity.this.pan.setText(pan);
                            MainActivity.this.tradeName.setText(detail.TradeName);
                            pf.clearAll();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            serv.SearchPANDetailsAsync(Constants.IRD_CLIENT_ID, Constants.IRD_TOKEN, pan);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setLanguage() {
        if (pf.getPreference("pref_lang").equals("Nepali")) {
            Locale locale = new Locale("ne");
            Utils.log(pf.getPreference("pref_lang"));
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        } else {
            Locale locale = new Locale("en");
            Utils.log(pf.getPreference("pref_lang"));
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
    }

    private void setUpDrawer() {
        Utils.log("Preference = " + pf.getPreference("pan"));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if (drawer != null) {
            drawer.setDrawerListener(toggle);
        }
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        try {
            toggle.syncState();
        }catch(Exception e){
            e.printStackTrace();
        }
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }
        login_btn = (Button) navigationView.getHeaderView(0).findViewById(R.id.login_btn);
        pan = (TextView) navigationView.getHeaderView(0).findViewById(R.id.pan_number);
        tradeName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.trade_name);
        isLoggedIn = pf.getBoolPreference("isLoggedIn", false);
        if (isLoggedIn) {
            showItem();
        } else {
            hideItem();
        }
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pf.getPreference("pan").equals("")) {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
            }
        });
    }


    private void hideItem() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_logout).setVisible(false);
        login_btn.setVisibility(View.VISIBLE);
        if (findViewById(R.id.row_tax_payer_portal) != null) {
            findViewById(R.id.row_tax_payer_portal).setVisibility(View.GONE);
        }
        if (findViewById(R.id.row_tds_portal) != null) {
            findViewById(R.id.row_tds_portal).setVisibility(View.GONE);
        }
        if (findViewById(R.id.row_office) != null) {
            findViewById(R.id.row_office).setVisibility(View.GONE);
        }
        pf.setBoolPreference("isLoggedIn", false);
        pf.setBoolPreference(Constants.LOGIN_TDS, false);
        pf.setBoolPreference(Constants.LOGIN_TAXPAYER, false);
        pf.setPreference("pan", "");
        pf.setPreference("trade_name", "");
        pan.setText(getResources().getString(R.string.ird));
        tradeName.setText(getResources().getString(R.string.quote));
        isLoggedIn = false;

    }

    private void showItem() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_logout).setVisible(true);
        if (pf.getBoolPreference(Constants.LOGIN_TDS, false)) {
            Utils.log("LOGIN_TDS = " + pf.getBoolPreference(Constants.LOGIN_TDS, false));
            findViewById(R.id.row_tds_portal).setVisibility(View.VISIBLE);
            findViewById(R.id.row_tax_payer_portal).setVisibility(View.GONE);
            findViewById(R.id.row_office).setVisibility(View.GONE);
            findViewById(R.id.row_tds_portal).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, WithHoldeeTransactionActivity.class));
                }
            });
        } else if (pf.getBoolPreference(Constants.LOGIN_TAXPAYER, false)) {
            Utils.log("LOGIN_TAX PAYER = " + pf.getBoolPreference(Constants.LOGIN_TAXPAYER, false));
            findViewById(R.id.row_tax_payer_portal).setVisibility(View.VISIBLE);
            findViewById(R.id.row_tds_portal).setVisibility(View.GONE);
            findViewById(R.id.row_office).setVisibility(View.GONE);

        } else if (pf.getBoolPreference(Constants.LOGIN_OFFICE, false)) {
            findViewById(R.id.row_tds_portal).setVisibility(View.GONE);
            findViewById(R.id.row_office).setVisibility(View.VISIBLE);
            findViewById(R.id.row_tax_payer_portal).setVisibility(View.GONE);
            findViewById(R.id.row_office).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, CollectionActivity.class));
                }
            });
        }
        login_btn.setVisibility(View.GONE);
        isLoggedIn = true;
        handleRequests();
        pan.setText(pf.getPreference("pan").isEmpty() ? getResources().getString(R.string.ird) : pf.getPreference("pan"));
        if (pf.getPreference("trade_name").equals("")) {
            if (!pf.getBoolPreference(Constants.LOGIN_OFFICE)) {
                getRegistrationDetail(pf.getPreference("pan"));
            }
        } else {
            tradeName.setText(pf.getPreference("trade_name"));
        }
    }

    private void initializeNotificationBar() {
        ll_notification = (LinearLayout) findViewById(R.id.notificationRow);
        close_button = (ImageView) findViewById(R.id.closeButton);
        notification = (TextView) findViewById(R.id.notification);
        if (pf.getPreference("notification").length() > 0) {
            String ntf = pf.getPreference("notification");

            try {
                final JSONObject obj = new JSONObject(ntf);
                notification.setText(obj.optString("message"));
                ll_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String data = obj.optString("data");
                        Intent notificationIntent;
                        if (data == null || data.length() == 0) {
                            notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
                        } else {
                            notificationIntent = new Intent(Intent.ACTION_VIEW);
                            notificationIntent.setData(Uri.parse(data));
                        }
                        startActivity(notificationIntent);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {
            ll_notification.setVisibility(View.GONE);
        }
        close_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_notification.setVisibility(View.GONE);
            }
        });

    }

    private void initializeViews() {

        findViewById(R.id.cardFaqs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), FAQsActivity.class));
            }
        });

        findViewById(R.id.cardEduMaterials).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), PublicationsActivity.class));
            }
        });

        findViewById(R.id.cardTaxLaws).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), TaxLawsActivity.class));
            }
        });
        findViewById(R.id.cardContacts).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), ContactActivity.class));
            }
        });
        findViewById(R.id.cardTaxCalculator).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), TaxCalculatorActivity.class));
            }
        });
        findViewById(R.id.cardNotices).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), NoticeActivity.class));
            }
        });
        txt_pan = (EditText) findViewById(R.id.text_pan);
        findViewById(R.id.btn_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNetworkConnected(getApplicationContext())) {
                    txt_pan.setError("No internet connection");
                } else if (txt_pan.getText().length() > 0) {
                    Intent i = new Intent(getApplicationContext(), PanSearchAcitivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("pan", txt_pan.getText().toString());
                    startActivity(i);
                } else {
                    txt_pan.setError("Invalid PAN Number");
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
        }
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Utils.toast(this, "Press back again to exit!!");
        }
        back_pressed = System.currentTimeMillis();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(this, "Download funtion will not work. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    private void checkPermissionGranted() {
        boolean hasPermission = (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_contact) {
            startActivity(new Intent(this, ContactActivity.class));
        } else if (id == R.id.nav_notice) {
            startActivity(new Intent(this, NoticeActivity.class));
        } else if (id == R.id.nav_faq) {
            startActivity(new Intent(this, FAQsActivity.class));
        } else if (id == R.id.nav_edu_mat) {
            startActivity(new Intent(this, PublicationsActivity.class));
        } else if (id == R.id.nav_tax_laws) {
            startActivity(new Intent(this, TaxLawsActivity.class));
        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(this, SettingsActivity.class));
        } else if (id == R.id.nav_logout) {
            hideItem();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    public void populate() {
        Utils.log("Populate");
        ll_notice = (LinearLayout) findViewById(R.id.ll_notice);
        View child_header = getLayoutInflater().inflate(R.layout.row_updates, null);
        seeAllUpdates = (TextView) child_header.findViewById(R.id.seeAllUpdates);
        child_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, UpdatesActivity.class));
            }
        });
        ll_notice.addView(child_header);
        if (noticeList != null) {
            for (int i = 0; i < (noticeList.size() < 3 ? noticeList.size() : 3); i++) {
                final int finalI = i;
                Utils.log("loop");
                View child = getLayoutInflater().inflate(R.layout.row_notice, null);
                TextView Heading = (TextView) child.findViewById(R.id.notice);
                TextView date = (TextView) child.findViewById(R.id.date);
                final ImageView Download = (ImageView) child.findViewById(R.id.downloadNotice);
                Heading.setText(noticeList.get(i).Heading);
                date.setText(Utils.getRelativeTime(Long.parseLong(noticeList.get(i).DatedOn) * 1000));
                Download.setImageResource((noticeList.get(finalI).isDownloaded ? R.drawable.eye : R.drawable.downoad));
                child.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        downloadTask(finalI, Download);
                    }
                });

                ll_notice.addView(child);
            }
        }
    }

    private void downloadTask(int finalI, final ImageView Download) {
        Utils.getUrl(noticeList.get(finalI).Url);
        if (noticeList.get(finalI).isDownloaded) {
            Utils.log(Utils.getSDPathFromUrl(noticeList.get(finalI).Url).toString());
            MyDownloader.readPdfFile(MainActivity.this, Utils.getSDPathFromUrl(noticeList.get(finalI).Url));
        } else if (!Utils.isNetworkConnected(getApplicationContext())) {
            Utils.snack(ll_notice.getRootView(), "No internet connection");

        } else {
            String url = Utils.getUrl(noticeList.get(finalI).Url);
            if (url != null) {
                new MyDownloader(this, finalI, url, new MyDownloader.onDownloadCompleteListner() {
                    @Override
                    public void downloadListener(int index, String link, boolean completed) {
                        if (completed) {

                            noticeList.get(index).isDownloaded = true;
                            Download.setImageResource(R.drawable.eye);
                        }
                    }
                }).execute();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.log("On resume");
    }

    @Override
    public void onSuccess(String table_name, String result) {
        ll_notice.removeAllViews();
        if (Utils.isNetworkConnected(this) && !pf.getPreference("result_notice").equals(result)) {
            pf.setPreference("result_notice", result);
        }
        if (result.length() > 0) {

            Utils.log("Rowsun:" + result);

            List<Notice> list = Notice.getNoticeList(result);
            noticeList.addAll(list);
            populate();
            Utils.log(list.get(0).Heading);
        }
    }

    @Override
    public void onError(String table_name, int status, String message) {
        Utils.log(message);
        onSuccess("", pf.getPreference("result_notice"));
    }
}
