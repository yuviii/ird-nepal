package com.mantraideas.irdnepal.activities;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.adapters.AdapterAttachment;
import com.mantraideas.irdnepal.helpers.DataQuery;
import com.mantraideas.irdnepal.helpers.DatabaseHelper;
import com.mantraideas.irdnepal.helpers.OnDataReceived;
import com.mantraideas.irdnepal.models.Attachments;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NoticeActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.notices));
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notice, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment implements OnDataReceived {
        private static final String ARG_SECTION_NUMBER = "section_number";
        private RecyclerView rv_notice_view;
        private List<Attachments> noticeList;
        private DatabaseHelper db;
        private TextView nodata;
        private DataQuery dq;
        private AdapterAttachment mAdapter;
        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(String sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_notice, container, false);
            return rootView;
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            rv_notice_view = (RecyclerView) getView().findViewById(R.id.rv_notice_view);
            rv_notice_view.setLayoutManager(new LinearLayoutManager(getActivity()));
            noticeList = new ArrayList<>();
            db = new DatabaseHelper(getActivity());
            noticeList = db.getAttachments(getArguments().getString(ARG_SECTION_NUMBER), null);
            mAdapter =  new AdapterAttachment(getActivity(),noticeList);
            rv_notice_view.setAdapter(mAdapter);
            nodata = (TextView) getView().findViewById(R.id.nodata);
            if(noticeList.size()==0){
                if (nodata != null) {
                    nodata.setVisibility(View.VISIBLE);
                }
                dq = new DataQuery(getContext(), this);
                dq.getRequestData(Constants.BASE_URL_ATTATCHMENTS);

            }
        }

        @Override
        public void onSuccess(String table_name, String result) {

            try {
                JSONArray jsonArray = new JSONArray(result);
                if(jsonArray.length()>0){
                    if (nodata != null) {
                        nodata.setVisibility(View.INVISIBLE);
                    }
                }else {

                    if (nodata != null) {
                        nodata.setVisibility(View.VISIBLE);
                    }
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
//                    Utils.log("Attachments"+object.toString());
                    if (!db.isExists(DBConstants.TABLE_ATTACHMENTS, object.optString(DBConstants.COLUMN_ATTACHMENT_ID))) {
                        db.addAttachment(object);
                    }
                    if (Utils.contains(object, DBConstants.COLUMN_CONTENT_ID)) {
                        if (object.optString(DBConstants.COLUMN_CONTENT_ID).equals(getArguments().getString(ARG_SECTION_NUMBER))) {
                            Attachments a = new Attachments(object);

                            this.noticeList.add(a);
                            Utils.log("match :" + object.toString());
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mAdapter.notifyDataSetChanged();

        }

        @Override
        public void onError(String table_name, int status, String message) {
            if (nodata != null) {
                nodata.setVisibility(View.VISIBLE);
            }
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        String[] tabs;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs = getResources().getStringArray(R.array.notice_tab);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(Constants.ID_NOTICE[position]);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return tabs.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }
    }
}
