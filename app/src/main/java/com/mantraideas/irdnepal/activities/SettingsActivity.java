package com.mantraideas.irdnepal.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.SyncService;
import com.mantraideas.irdnepal.utilities.Utils;

import java.util.Locale;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingFragment())
                .commit();
        setTitle(getString(R.string.action_settings));
    }

    public static class SettingFragment extends PreferenceFragment {
        com.mantraideas.irdnepal.helpers.Preference pf;

        @Override

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref);
            Preference preference = findPreference("pref_version");
            preference.setTitle("Version - " + Utils.getVersionName(getActivity()));
            preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Utils.doRate(getActivity());
                    return true;
                }
            });
            Preference psync = findPreference("pref_sync");
            psync.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    getActivity().startService(new Intent(getActivity(), SyncService.class));
                    Utils.toast(getActivity(), "Syncing Data. Please wait...");
                    return true;
                }
            });
            pf = new com.mantraideas.irdnepal.helpers.Preference(getActivity());
            preference = findPreference("pref_lang");
            preference.setSummary(pf.getPreference("pref_lang"));
            preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    setLang();
                    preference.setSummary((String) newValue);
                    return true;
                }
            });
        }

        private void setLang() {
            if (pf.getPreference("pref_lang").equals("Nepali")) {
                Locale locale = new Locale("ne");
                Utils.log(pf.getPreference("pref_lang"));
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getActivity().getBaseContext().getResources().updateConfiguration(config,
                        getActivity().getBaseContext().getResources().getDisplayMetrics());
            } else {
                Locale locale = new Locale("en");
                Utils.log(pf.getPreference("pref_lang"));
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getActivity().getBaseContext().getResources().updateConfiguration(config,
                        getActivity().getBaseContext().getResources().getDisplayMetrics());
            }

            startActivity(getActivity().getIntent());
        }

    }

    @Override
    public void finish() {
        super.finish();
        startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}
