package com.mantraideas.irdnepal.activities.taxpayerportal;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.soap.Collection;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.OperationResult;
import com.mantraideas.irdnepal.soap.Payment;
import com.mantraideas.irdnepal.soap.SMSResponse;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.Utils;

public class CollectionActivity extends AppCompatActivity {
    Preference pf;
    ProgressDialog pg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);
        initialize();
        pf = new Preference(this);
        pg = new ProgressDialog(this);
        pg.setMessage("Loading....");
        showAlert();
    }

    private void initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.alert_collection, null, false);
        final EditText fromDate= (EditText) view.findViewById(R.id.from);
        final EditText toDate = (EditText) view.findViewById(R.id.to);
        view.setPadding(16,16,16,16);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callServer(fromDate.getText().toString(),toDate.getText().toString());
            }
        }).setTitle("Please enter date:").setView(view).setCancelable(false).show();
    }

    private void callServer(String fromDate, String toDate) {
        IIntegratedTaxSystemService service = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                pg.show();
            }

            @Override
            public void Completed(OperationResult result) {
                pg.dismiss();
                try {
                    SMSResponse response = (SMSResponse) result.Result;
                    if (response != null) {
                        if (response.Collection != null) {
                            Collection data = response.Collection;
                            if (data.Vat == null) {
                                Snackbar.make(findViewById(R.id.wrapper), "No data available", Snackbar.LENGTH_INDEFINITE).show();
                            }
                            ((TextView) findViewById(R.id.txt_vat)).setText(data.Vat==null||data.Vat.isEmpty()?"N/A":data.Vat);
                            ((TextView) findViewById(R.id.txt_income_tax)).setText(data.IncomeTax==null||data.IncomeTax.isEmpty()?"N/A":data.IncomeTax);
                            ((TextView) findViewById(R.id.txt_excise)).setText(data.Excise==null||data.Excise.isEmpty()?"N/A":data.Excise);
                        }
                    }


                }catch (Exception e){
                    Snackbar.make(findViewById(R.id.wrapper), "Connection error.", Snackbar.LENGTH_LONG).show();
                }

            }
        });
        service.GetCollectionForMobileAsync(Constants.IRD_CLIENT_ID, Constants.IRD_TOKEN,fromDate,toDate);
    }
}
