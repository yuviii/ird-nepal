package com.mantraideas.irdnepal.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.DatabaseHelper;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TaxReportActivity extends AppCompatActivity {
    TextView acc_amount, tax_amount;
    LinearLayout ll_tax_report;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tax_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        JSONObject obj = new JSONObject();
        int index = getIntent().getIntExtra("type", -1);
        if (index == -1) {
            finish();
        }
        String intentYear = getIntent().getStringExtra("year");

        db = new DatabaseHelper(this);
        JSONArray config = db.getDataInJSON(DBConstants.TABLE_TAX_RATE);
        for (int i = 0; i < config.length(); i++) {
            try {
                obj = config.getJSONObject(i);
                String year = obj.optString("year");
                if (year.equals(intentYear)) {
                    break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

//        Utils.log("config  = " + config);

        Double income = getIntent().getDoubleExtra("income", 0.0);
        Double deduction = getIntent().getDoubleExtra("deduction", 0.0);
        acc_amount = (TextView) findViewById(R.id.total_acc_amount);
        tax_amount = (TextView) findViewById(R.id.total_tax);
        ll_tax_report = (LinearLayout) findViewById(R.id.ll_report);


        if (obj.length() > 0) {
            String value = obj.optString("value");
            try {
                JSONObject ob = new JSONObject(value);
                JSONArray array = ob.getJSONArray(Constants.TAX_RATE_TYPE[index]);
                calc(income, deduction, array);
                Utils.log(Constants.TAX_RATE_TYPE[index] + "  " + ob.get(Constants.TAX_RATE_TYPE[index]) + "");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //calculate(income, deduction);


    }


    public void calc(Double income, Double deduction, JSONArray value) throws JSONException {
        Double total = income - deduction;
        Double tax;
        JSONObject obj1 = value.getJSONObject(0);
        JSONObject obj2 = value.getJSONObject(1);
        JSONObject obj3 = value.getJSONObject(2);
        Double FROM1, RATE1, TO1;
        Double FROM2, RATE2, TO2;
        Double FROM3, RATE3, TO3;
        FROM1 = Double.parseDouble(obj1.optString(Constants.FROM));
        RATE1 = Double.parseDouble(obj1.optString(Constants.RATE));
        TO1 = Double.parseDouble(obj1.optString(Constants.TO));
        FROM2 = Double.parseDouble(obj2.optString(Constants.FROM));
        RATE2 = Double.parseDouble(obj2.optString(Constants.RATE));
        TO2 = Double.parseDouble(obj2.optString(Constants.TO));
        FROM3 = Double.parseDouble(obj3.optString(Constants.FROM));
        RATE3 = Double.parseDouble(obj3.optString(Constants.RATE));
        TO3 = Double.parseDouble(obj3.optString(Constants.TO));

        if (total < TO1) {
            tax = total * RATE1 / 100;
            acc_amount.setText("Total = " + String.format("Rs. %.0f", total) + "");
            tax_amount.setText(String.format("Rs. %.0f", tax) + "");
            View child = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income = (TextView) child.findViewById(R.id.acc_income);
            TextView rate = (TextView) child.findViewById(R.id.rate);
            TextView tax_amt = (TextView) child.findViewById(R.id.tax);
            acc_income.setText(String.format("Rs. %.0f", total));
            rate.setText(RATE1+"");
            tax_amt.setText(String.format("Rs. %.0f", tax));
            ll_tax_report.addView(child);
            Utils.log("Tax=" + tax.toString() + ", Accessible amount = " + total.toString());

        } else if (total > TO1 && total < TO2) {
            tax = ((total - FROM2) * RATE2/ 100) + (FROM2*RATE1/100);
            //rate 1%
            View child = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income = (TextView) child.findViewById(R.id.acc_income);
            TextView rate = (TextView) child.findViewById(R.id.rate);
            TextView tax_amt = (TextView) child.findViewById(R.id.tax);
            acc_income.setText(FROM2 + "");
            rate.setText(RATE1+"");
            tax_amt.setText((FROM2*RATE1/100)+"");
            ll_tax_report.addView(child);
            //rate 15 %
            View child1 = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income1 = (TextView) child1.findViewById(R.id.acc_income);
            TextView rate1 = (TextView) child1.findViewById(R.id.rate);
            TextView tax_amt1 = (TextView) child1.findViewById(R.id.tax);
            acc_income1.setText(String.format("Rs. %.0f", (total - FROM2)) + "");
            rate1.setText(RATE2+"");
            tax_amt1.setText(String.format("Rs. %.0f", (total - FROM2) * RATE2/ 100) + "");
            ll_tax_report.addView(child1);
            acc_amount.setText("Total = " + String.format("Rs. %.0f", total) + "");
            tax_amount.setText(String.format("Rs. %.0f", tax) + "");

        }else if (total > FROM3) {
            tax = ((total - FROM3) * RATE3/ 100) + (FROM2*RATE1/100) + (((TO2-FROM2)*RATE2)/100);

            //rate 1%
            View child = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income = (TextView) child.findViewById(R.id.acc_income);
            TextView rate = (TextView) child.findViewById(R.id.rate);
            TextView tax_amt = (TextView) child.findViewById(R.id.tax);
            acc_income.setText("Rs. " + TO1);
            rate.setText(RATE1  +"");
            tax_amt.setText("Rs. " + (FROM2*RATE1/100) );
            ll_tax_report.addView(child);
            //rate 15 %
            View child1 = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income1 = (TextView) child1.findViewById(R.id.acc_income);
            TextView rate1 = (TextView) child1.findViewById(R.id.rate);
            TextView tax_amt1 = (TextView) child1.findViewById(R.id.tax);
            acc_income1.setText("Rs. " + (TO2-FROM2));
            rate1.setText(RATE2 + "");
            tax_amt1.setText("Rs. " + (((TO2-FROM2)*RATE2)/100) );
            ll_tax_report.addView(child1);

            //rate 25%
            View child2 = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income2 = (TextView) child2.findViewById(R.id.acc_income);
            TextView rate2 = (TextView) child2.findViewById(R.id.rate);
            TextView tax_amt2 = (TextView) child2.findViewById(R.id.tax);
            acc_income2.setText(String.format("Rs. %.0f", (total - FROM3)) + "");
            rate2.setText(RATE3 + "");
            tax_amt2.setText(String.format("Rs. %.0f", (total - FROM3) * RATE3/ 100) + "");
            ll_tax_report.addView(child2);


            acc_amount.setText("Total = " + String.format("Rs. %.0f", total) + "");
            tax_amount.setText(String.format("Rs. %.0f", tax) + "");
        }


    }


    public void calculate(Double income, Double deduction) {
        Double total = income - deduction;
        Double tax;
        if (total < 250000) {

            tax = total * 1 / 100;
            acc_amount.setText("Total = " + String.format("Rs. %.0f", total) + "");
            tax_amount.setText(String.format("Rs. %.0f", tax) + "");
            View child = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income = (TextView) child.findViewById(R.id.acc_income);
            TextView rate = (TextView) child.findViewById(R.id.rate);
            TextView tax_amt = (TextView) child.findViewById(R.id.tax);
            acc_income.setText(String.format("Rs. %.0f", total));
            rate.setText("1");
            tax_amt.setText(String.format("Rs. %.0f", tax));
            ll_tax_report.addView(child);
            Utils.log("Tax=" + tax.toString() + ", Accessible amount = " + total.toString());

        } else if (total > 250000 && total < 350000) {
            tax = ((total - 250000) * 15 / 100) + 2500;
            //rate 1%
            View child = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income = (TextView) child.findViewById(R.id.acc_income);
            TextView rate = (TextView) child.findViewById(R.id.rate);
            TextView tax_amt = (TextView) child.findViewById(R.id.tax);
            acc_income.setText("250000");
            rate.setText("1");
            tax_amt.setText("2500");
            ll_tax_report.addView(child);
            //rate 15 %
            View child1 = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income1 = (TextView) child1.findViewById(R.id.acc_income);
            TextView rate1 = (TextView) child1.findViewById(R.id.rate);
            TextView tax_amt1 = (TextView) child1.findViewById(R.id.tax);
            acc_income1.setText(String.format("Rs. %.0f", (total - 250000)) + "");
            rate1.setText("15");
            tax_amt1.setText(String.format("Rs. %.0f", (total - 250000) * 15 / 100) + "");
            ll_tax_report.addView(child1);


            acc_amount.setText("Total = " + String.format("Rs. %.0f", total) + "");
            tax_amount.setText(String.format("Rs. %.0f", tax) + "");

        } else if (total > 350000) {
            tax = ((total - 350000) * 25 / 100) + 2500 + 15000;

            //rate 1%
            View child = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income = (TextView) child.findViewById(R.id.acc_income);
            TextView rate = (TextView) child.findViewById(R.id.rate);
            TextView tax_amt = (TextView) child.findViewById(R.id.tax);
            acc_income.setText("Rs. 250000");
            rate.setText("1");
            tax_amt.setText("Rs. 2500");
            ll_tax_report.addView(child);
            //rate 15 %
            View child1 = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income1 = (TextView) child1.findViewById(R.id.acc_income);
            TextView rate1 = (TextView) child1.findViewById(R.id.rate);
            TextView tax_amt1 = (TextView) child1.findViewById(R.id.tax);
            acc_income1.setText("Rs. 100000");
            rate1.setText("15");
            tax_amt1.setText("Rs. 15000");
            ll_tax_report.addView(child1);

            //rate 25%
            View child2 = getLayoutInflater().inflate(R.layout.row_tax_report, null);
            TextView acc_income2 = (TextView) child2.findViewById(R.id.acc_income);
            TextView rate2 = (TextView) child2.findViewById(R.id.rate);
            TextView tax_amt2 = (TextView) child2.findViewById(R.id.tax);
            acc_income2.setText(String.format("Rs. %.0f", (total - 350000)) + "");
            rate2.setText("25");
            tax_amt2.setText(String.format("Rs. %.0f", (total - 350000) * 25 / 100) + "");
            ll_tax_report.addView(child2);


            acc_amount.setText("Total = " + String.format("Rs. %.0f", total) + "");
            tax_amount.setText(String.format("Rs. %.0f", tax) + "");
        }
    }

}
