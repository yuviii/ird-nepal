package com.mantraideas.irdnepal.activities.taxpayerportal;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.soap.Collection;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.OperationResult;
import com.mantraideas.irdnepal.soap.SMSResponse;
import com.mantraideas.irdnepal.soap.TDSResponse;
import com.mantraideas.irdnepal.soap.WithholdeeTransaction;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.Utils;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class WithHoldeeTransactionActivity extends AppCompatActivity {
    Preference pf;
    ProgressDialog pg;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_holdee_transaction);
        initialize();
        pf = new Preference(this);
        pg = new ProgressDialog(this);
        pg.setMessage("Loading....");
        list = (ListView) findViewById(R.id.list);
        showAlert();
    }

    private void initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.alert_collection, null, false);
        final EditText fromDate = (EditText) view.findViewById(R.id.from);
        final EditText toDate = (EditText) view.findViewById(R.id.to);
        view.setPadding(16, 16, 16, 16);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callServer(fromDate.getText().toString(), toDate.getText().toString());
            }
        }).setTitle("Please enter date:").setView(view).setCancelable(false).show();
    }

    private void callServer(String fromDate, String toDate) {
        IIntegratedTaxSystemService service = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                pg.show();
            }

            @Override
            public void Completed(OperationResult result) {
                Utils.log("Response=" + result.Result);
                pg.dismiss();
                try {
                    TDSResponse tds = (TDSResponse) result.Result;
                    if (tds.WithholdeeTransactions.size() <= 0) {
//                        Snackbar.make(findViewById(R.id.fab),"No data available",Snackbar.LENGTH_INDEFINITE).show();
                        findViewById(R.id.nodata).setVisibility(View.VISIBLE);
                    } else {
                        findViewById(R.id.nodata).setVisibility(View.GONE);
                    }
                    ArrayAdapter<WithholdeeTransaction> aa = new ArrayAdapter<WithholdeeTransaction>(WithHoldeeTransactionActivity.this, R.layout.simple_list, tds.WithholdeeTransactions) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View view = LayoutInflater.from(WithHoldeeTransactionActivity.this).inflate(R.layout.simple_list, null);
                            ((TextView) view.findViewById(R.id.name)).setText("With Holder Name : " + (getItem(position).WithHolderName == null ? "N/A" : getItem(position).WithHolderName));
                            ((TextView) view.findViewById(R.id.date)).setText("VerifiedDate : " + (getItem(position).VerifiedDate==null? "N/A" : getItem(position).VerifiedDate));
                            ((TextView) view.findViewById(R.id.amount)).setText("Amount : " + getItem(position).Amount + "");
                            ((TextView) view.findViewById(R.id.tds_amount)).setText("TDS Amount : " + getItem(position).TDSAmount+ "");
                            ((TextView) view.findViewById(R.id.pan)).setText("PAN : " + (getItem(position).WithHolderPAN == null ? "N/A" : getItem(position).WithHolderPAN));

                            return view;
                        }
                    };
                    list.setAdapter(aa);
                } catch (Exception e) {
                    findViewById(R.id.nodata).setVisibility(View.VISIBLE);
                    Snackbar.make(findViewById(R.id.fab), "No data available", Snackbar.LENGTH_INDEFINITE).show();
                }

            }
        });
        service.GetWithHoldeeTransactionsAsync(Constants.IRD_CLIENT_ID, Constants.IRD_TOKEN, pf.getPreference("pan"), fromDate, toDate);
    }

}
