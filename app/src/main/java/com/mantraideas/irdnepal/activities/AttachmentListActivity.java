package com.mantraideas.irdnepal.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.adapters.AdapterAttachment;
import com.mantraideas.irdnepal.helpers.DataQuery;
import com.mantraideas.irdnepal.helpers.DatabaseHelper;
import com.mantraideas.irdnepal.helpers.OnDataReceived;
import com.mantraideas.irdnepal.models.Attachments;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AttachmentListActivity extends AppCompatActivity //implements OnDataReceived
{
    RecyclerView rv_attachment_list;
    List<Attachments> attachmentsList;
    AdapterAttachment mAdapter;
    DataQuery dq;
    DatabaseHelper db;
    String contentid;
    TextView nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attatchment_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            setTitle(getIntent().getStringExtra("title"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        contentid = getIntent().getStringExtra("contentId");
        attachmentsList = new ArrayList<>();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rv_attachment_list = (RecyclerView) findViewById(R.id.rv_attachment_list);
        rv_attachment_list.setLayoutManager(new LinearLayoutManager(this));
        db = new DatabaseHelper(this);
        attachmentsList = db.getAttachments(contentid, null);
        mAdapter = new AdapterAttachment(this, attachmentsList);
        rv_attachment_list.setAdapter(mAdapter);
        nodata = (TextView) findViewById(R.id.nodata);
        if (attachmentsList.size() == 0) {
            if (nodata != null) {
                nodata.setVisibility(View.VISIBLE);
            }
       //     request();
        } else {
            nodata.setVisibility(View.INVISIBLE);
        }
    }

//    void request() {
//        dq = new DataQuery(this, this);
//        dq.getRequestData(Constants.BASE_URL_ATTATCHMENTS);
//
//    }

//    @Override
//    public void onSuccess(String table_name, String result) {
//        try {
//            JSONArray jsonArray = new JSONArray(result);
//            if(jsonArray.length()>0){
//                if (nodata != null) {
//                    nodata.setVisibility(View.INVISIBLE);
//                }
//            }else {
//
//                if (nodata != null) {
//                    nodata.setVisibility(View.VISIBLE);
//                }
//            }
//             for (int i = 0; i < jsonArray.length(); i++) {
//                JSONObject object = jsonArray.getJSONObject(i);
////                    Utils.log("Attachments"+object.toString());
//                if (!db.isExists(DBConstants.TABLE_ATTACHMENTS, object.optString(DBConstants.COLUMN_ATTACHMENT_ID))) {
//                    db.addAttachment(object);
//                }
//                if (Utils.contains(object, DBConstants.COLUMN_CONTENT_ID)) {
//                    Utils.log("contains ");
//                    if (object.optString(DBConstants.COLUMN_CONTENT_ID).equals(contentid)) {
//
//                        Attachments a = new Attachments(object);
//
//                        this.attachmentsList.add(a);
//                        Utils.log("match :" + object.toString());
//                    }
//
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        mAdapter.notifyDataSetChanged();
//
//    }
//
//
//    @Override
//    public void onError(String table_name, int status, String message) {
//        nodata.setVisibility(View.VISIBLE);
//    }
}
