package com.mantraideas.irdnepal.activities;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.UtteranceProgressListener;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.DatabaseHelper;
import com.mantraideas.irdnepal.helpers.SyncService;
import com.mantraideas.irdnepal.soap.ArrayOfTaxpayer;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TaxCalculatorActivity extends AppCompatActivity {
    Spinner spn_tax_payer_type;
    Spinner spn_year;
    Button advance_option, calculate_tax;
    LinearLayout more_items;
    Double income, deduction;
    EditText salary, bonus, overtime, eta, leave_pay, prize, facilitations, dearness_allowance, cost_of_living, rent_allowances, other_1, other_2, other_3, other_4, other_5, other_6, other_7, other_8, other_9, other_10, other_11, other_12;
    EditText provident_fund, ctzn_investment_trust, insurance;
    DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tax_calculator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        db = new DatabaseHelper(this);
        final List<String> year= new ArrayList<>();
        JSONArray config = db.getDataInJSON(DBConstants.TABLE_TAX_RATE);
        for(int i = 0 ; i <config.length();i++){
            try {
                JSONObject obj = config.getJSONObject(i);
                year.add(obj.optString("year"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(config.length()<=0){
            startService(new Intent(this, SyncService.class));
        }
        
        //spinners
        spn_tax_payer_type = (Spinner) findViewById(R.id.tax_payer_type);
        ArrayAdapter<CharSequence> adapter_type = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.tax_payer_type, android.R.layout.simple_spinner_item);
        adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_tax_payer_type.setAdapter(adapter_type);
        spn_year = (Spinner) findViewById(R.id.year);
//        ArrayAdapter<CharSequence> adapter_year = ArrayAdapter.createFromResource(getApplicationContext(),year, android.R.layout.simple_spinner_item);
        ArrayAdapter<String> adapter_year = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item,year);
        adapter_year.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_year.setAdapter(adapter_year);
        income = 0.0;
        //buttons
        calculate_tax = (Button) findViewById(R.id.calculate);
        advance_option = (Button) findViewById(R.id.more);
        //linearLayout
        more_items = (LinearLayout) findViewById(R.id.more_items);

        //Annual Income
        salary = (EditText) findViewById(R.id.salary);
        bonus = (EditText) findViewById(R.id.bonus);
        eta = (EditText) findViewById(R.id.eta);
        leave_pay = (EditText) findViewById(R.id.leave_pay);
        overtime = (EditText) findViewById(R.id.overtime);
        prize = (EditText) findViewById(R.id.prize);
        facilitations = (EditText) findViewById(R.id.facilitations);
        dearness_allowance = (EditText) findViewById(R.id.dearness_allowance);
        cost_of_living = (EditText) findViewById(R.id.cost_of_living);
        rent_allowances = (EditText) findViewById(R.id.rent_allowances);
        other_1 = (EditText) findViewById(R.id.other_1);
        other_2 = (EditText) findViewById(R.id.other_2);
        other_3 = (EditText) findViewById(R.id.other_3);
        other_4 = (EditText) findViewById(R.id.other_4);
        other_5 = (EditText) findViewById(R.id.other_5);
        other_6 = (EditText) findViewById(R.id.other_6);
        other_7 = (EditText) findViewById(R.id.other_7);
        other_8 = (EditText) findViewById(R.id.other_8);
        other_9 = (EditText) findViewById(R.id.other_9);
        other_10 = (EditText) findViewById(R.id.other_10);
        other_11 = (EditText) findViewById(R.id.other_11);
        other_12 = (EditText) findViewById(R.id.other_12);
        //Annual Deduction
        provident_fund = (EditText) findViewById(R.id.provident_fund);
        ctzn_investment_trust = (EditText) findViewById(R.id.ctzn_investment_trust);
        insurance = (EditText) findViewById(R.id.insurance);

        advance_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_items.setVisibility(more_items.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
            }
        });
        calculate_tax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                income = 0.0;
                if (salary != null) {
                    income += Double.parseDouble(!salary.getText().toString().equals("") ? salary.getText().toString() : "0");
                }
                if (bonus != null) {
                    income += Double.parseDouble(!bonus.getText().toString().equals("") ? bonus.getText().toString() : "0");
                }
                if (eta != null) {
                    income += Double.parseDouble(!eta.getText().toString().equals("") ? eta.getText().toString() : "0");
                }
                if (leave_pay != null) {
                    income += Double.parseDouble(!leave_pay.getText().toString().equals("") ? leave_pay.getText().toString() : "0");
                }
                if (prize != null) {
                    income += Double.parseDouble(!prize.getText().toString().equals("") ? prize.getText().toString() : "0");
                }
                if (facilitations != null) {
                    income += Double.parseDouble(!facilitations.getText().toString().equals("") ? facilitations.getText().toString() : "0");
                }
                if (dearness_allowance != null) {
                    income += Double.parseDouble(!dearness_allowance.getText().toString().equals("") ? dearness_allowance.getText().toString() : "0");
                }
                if (cost_of_living != null) {
                    income += Double.parseDouble(!cost_of_living.getText().toString().equals("") ? cost_of_living.getText().toString() : "0");
                }
                if (rent_allowances != null) {
                    income += Double.parseDouble(!rent_allowances.getText().toString().equals("") ? rent_allowances.getText().toString() : "0");
                }
                if (overtime != null) {
                    income += Double.parseDouble(!overtime.getText().toString().equals("") ? overtime.getText().toString() : "0");
                }
                if (other_1 != null) {
                    income += Double.parseDouble(!other_1.getText().toString().equals("") ? other_1.getText().toString() : "0");
                }
                if (other_2 != null) {
                    income += Double.parseDouble(!other_2.getText().toString().equals("") ? other_2.getText().toString() : "0");
                }
                if (other_3 != null) {
                    income += Double.parseDouble(!other_3.getText().toString().equals("") ? other_3.getText().toString() : "0");
                }
                if (other_4 != null) {
                    income += Double.parseDouble(!other_4.getText().toString().equals("") ? other_4.getText().toString() : "0");
                }
                if (other_5 != null) {
                    income += Double.parseDouble(!other_5.getText().toString().equals("") ? other_5.getText().toString() : "0");
                }
                if (other_6 != null) {
                    income += Double.parseDouble(!other_6.getText().toString().equals("") ? other_6.getText().toString() : "0");
                }
                if (other_7 != null) {
                    income += Double.parseDouble(!other_7.getText().toString().equals("") ? other_7.getText().toString() : "0");
                }
                if (other_8 != null) {
                    income += Double.parseDouble(!other_8.getText().toString().equals("") ? other_8.getText().toString() : "0");
                }
                if (other_9 != null) {
                    income += Double.parseDouble(!other_9.getText().toString().equals("") ? other_9.getText().toString() : "0");
                }
                if (other_10 != null) {
                    income += Double.parseDouble(!other_10.getText().toString().equals("") ? other_10.getText().toString() : "0");
                }
                if (other_11 != null) {
                    income += Double.parseDouble(!other_11.getText().toString().equals("") ? other_11.getText().toString() : "0");
                }
                if (other_2 != null) {
                    income += Double.parseDouble(!other_12.getText().toString().equals("") ? other_12.getText().toString() : "0");
                }

                deduction = 0.0;
                if (provident_fund != null) {
                    deduction += Double.parseDouble(!provident_fund.getText().toString().equals("") ? provident_fund.getText().toString() : "0");
                }
                if (ctzn_investment_trust != null) {

                    if (!ctzn_investment_trust.getText().toString().equals("")) {
                        if (Double.parseDouble(ctzn_investment_trust.getText().toString()) > income / 3.0) {
                            ctzn_investment_trust.setError("Invalid amount, should be less than one third of total income.");
                            return;
                        } else {
                            deduction += Double.parseDouble(ctzn_investment_trust.getText().toString());
                        }
                    }
                }
                if (insurance != null) {

                    if (!insurance.getText().toString().equals("")) {
                        if (Double.parseDouble(insurance.getText().toString()) > 20000.0) {
                            insurance.setError("Invalid amount, should be less than 20001");
                            return;
                        } else {
                            deduction += Double.parseDouble(insurance.getText().toString());
                        }
                    }

                }

                Intent i =new Intent(v.getContext(), TaxReportActivity.class);
                i.putExtra("income",income);
                i.putExtra("deduction",deduction);
                i.putExtra("year",spn_year.getSelectedItem().toString());
                i.putExtra("type",spn_tax_payer_type.getSelectedItemPosition());
                if(year.size()>0) {
                    startActivity(i);
                }

            }
        });
    }

}
