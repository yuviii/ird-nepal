package com.mantraideas.irdnepal.activities.taxpayerportal;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.soap.Arrear;
import com.mantraideas.irdnepal.soap.EstimatedReturns;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.OperationResult;
import com.mantraideas.irdnepal.soap.SMSResponse;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.Utils;

public class ArrearActivity extends AppCompatActivity {
    Preference pf;
    ProgressDialog pg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arrear);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pg = new ProgressDialog(this);
        pg.setMessage("Loading ... ");
        pf = new Preference(this);
        callServer();
    }

    private void callServer() {
        IIntegratedTaxSystemService service = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
                pg.show();
            }

            @Override
            public void Completed(OperationResult result) {
                if(result==null){
                    new AlertDialog.Builder(ArrearActivity.this).setMessage("Connectionn error").show();
                    return;
                }
                SMSResponse response = (SMSResponse) result.Result;
                pg.dismiss();
                if (response != null) {
                    Arrear data = response.Arrear;
                    if(data.Vat==null&&data.Excise==null&&data.IncomeTax==null){
                        Snackbar.make(findViewById(R.id.txt_vat),"No data available",Snackbar.LENGTH_INDEFINITE).show();
                    }
                    Utils.log("VAT = "+data.Vat+"");
                    ((TextView)findViewById(R.id.txt_vat)).setText(data.Vat==null||data.Vat.isEmpty()?"N/A":data.Vat);
                    ((TextView)findViewById(R.id.txt_income_tax)).setText(data.IncomeTax==null||data.IncomeTax.isEmpty()?"N/A":data.IncomeTax);
                    ((TextView)findViewById(R.id.txt_excise)).setText(data.Excise==null||data.Excise.isEmpty()?"N/A":data.Excise);

                }else{
                    new AlertDialog.Builder(ArrearActivity.this).setMessage("Connectionn error").show();
                    finish();
                }
            }
        });
        service.GetArrearForMobileAsync(Constants.IRD_CLIENT_ID, Constants.IRD_TOKEN, pf.getPreference("pan"));
    }


}
