package com.mantraideas.irdnepal.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.adapters.AdapterNoticeView;
import com.mantraideas.irdnepal.helpers.DataQuery;
import com.mantraideas.irdnepal.helpers.OnDataReceived;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.models.Notice;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

public class UpdatesActivity extends AppCompatActivity implements OnDataReceived {
    RecyclerView mNoticeView;
    AdapterNoticeView mAdapter;
    List<Notice> noticeList;
    DataQuery dq;
    Preference pf;
   TextView nodata;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updates);
        setTitle(getResources().getString(R.string.current_updates));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        noticeList=new ArrayList<>();
        pf = new Preference(this);
        List<Notice> list = Notice.getNoticeList(pf.getPreference("notice_list"));
        noticeList.addAll(list);
        mNoticeView= (RecyclerView) findViewById(R.id.noticesView);
        mNoticeView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter=new AdapterNoticeView(this,noticeList);
        mNoticeView.setAdapter(mAdapter);

        nodata = (TextView) findViewById(R.id.nodata);
        if(Utils.isNetworkConnected(this)) {
            dq = new DataQuery(this, this);
            dq.getRequestData(Constants.BASE_URL_NOTICE);
        }else{
            onSuccess("",pf.getPreference("notice_list"));
        }
    }

    @Override
    public void onSuccess(String table_name, String result) {
        if (Utils.isNetworkConnected(this)) {
            pf.setPreference("notice_list",result);
        }if(result.length()>2){
            if (nodata != null) {
                nodata.setVisibility(View.INVISIBLE);
            }
        }else {
            if (nodata != null) {
                nodata.setVisibility(View.VISIBLE);
            }
        }
        if (result.length() > 2) {
            Utils.log("Rowsun:" + result);
            List<Notice> list = Notice.getNoticeList(result);
            noticeList.clear();
            noticeList.addAll(list);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onError(String table_name, int status, String message) {
        onSuccess("",pf.getPreference("notice_list"));

    }
}
