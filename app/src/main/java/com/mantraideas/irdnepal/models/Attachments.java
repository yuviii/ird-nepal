package com.mantraideas.irdnepal.models;

import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rowsun on 5/24/16.
 */
public class Attachments {
    public String ContentAttachmentId,ContentId,AttachmentUrl,LanguageInAttachMent,AttachmentCommentlanguage,Comment,Comment2,LastUpdateDate,Content;
    public boolean isDownloaded;

    public Attachments() {
    }

    public Attachments(String contentAttachmentId, String contentId, String attachmentUrl, String languageInAttachMent, String attachmentCommentlanguage, String comment, String comment2, String lastUpdateDate, String content) {
        ContentAttachmentId = contentAttachmentId;
        ContentId = contentId;
        AttachmentUrl = attachmentUrl;
        LanguageInAttachMent = languageInAttachMent;
        AttachmentCommentlanguage = attachmentCommentlanguage;
        Comment = comment;
        Comment2 = comment2;
        LastUpdateDate = lastUpdateDate;
        Content = content;
    }

    public static List<Attachments> getAttachmentsList(String string) {
        try {
            return getAttachmentsList(new JSONArray(string));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static List<Attachments> getAttachmentsList(JSONArray jArr) {
        List<Attachments> list = new ArrayList<>();
        try {
            for (int i = 0; i < jArr.length(); i++) {
                list.add(new Attachments(jArr.optJSONObject(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Attachments(JSONObject jObj) {
        this.ContentAttachmentId = jObj.optString("ContentAttachmentId");
        this.ContentId = jObj.optString("ContentId");
        this.AttachmentUrl = jObj.optString("AttachmentUrl");
        this.LanguageInAttachMent = jObj.optString("LanguageInAttachMent");
        this.AttachmentCommentlanguage = jObj.optString("AttachmentCommentlanguage");
        this.Comment = jObj.optString("Comment");
        this.Comment2 = jObj.optString("Comment2");
        this.LastUpdateDate = jObj.optString("LastUpdateDate");
        this.Content = jObj.optString("Content");
        this.isDownloaded = Utils.checkFileExist(this.AttachmentUrl);
    }
}
