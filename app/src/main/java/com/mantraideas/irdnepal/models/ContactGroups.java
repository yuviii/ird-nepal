package com.mantraideas.irdnepal.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rowsun on 5/27/16.
 */
public class ContactGroups {
  public String ContactGroupId,Name,Alias,IsVisible,CreatedOn,ContactList;

    public ContactGroups(String contactGroupId, String name, String alias, String isVisible, String createdOn, String contactList) {
        ContactGroupId = contactGroupId;
        Name = name;
        Alias = alias;
        IsVisible = isVisible;
        CreatedOn = createdOn;
        ContactList = contactList;
    }
    public static List<ContactGroups> getContactGroups(String string) {
        try {
            return getContactGroups(new JSONArray(string));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static List<ContactGroups> getContactGroups(JSONArray jArr) {
        List<ContactGroups> list = new ArrayList<>();
        try {
            for (int i = 0; i < jArr.length(); i++) {
                list.add(new ContactGroups(jArr.optJSONObject(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ContactGroups(JSONObject jObj) {
        this.ContactGroupId = jObj.optString("ContactGroupId");
        this.Name = jObj.optString("Name");
        this.Alias = jObj.optString("Alias");
        this.IsVisible = jObj.optString("IsVisible");
        this.CreatedOn = jObj.optString("CreatedOn");
        this.ContactList = jObj.optString("ContactList");
    }
}
