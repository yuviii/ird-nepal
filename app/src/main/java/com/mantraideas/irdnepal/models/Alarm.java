package com.mantraideas.irdnepal.models;

import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rowsun on 7/5/16.
 */
public class Alarm {
    public String id,date,message,type,month;
    public boolean flag;

    public Alarm(){}
    public Alarm(String id,String date, String message, String type, String month,boolean flag) {
        this.id = id;
        this.date = date;
        this.message = message;
        this.type = type;
        this.month = month;
        this.flag = flag;
    }

    public static List<Alarm> getAlarmList(String string) {
        try {
            return getAlarmList(new JSONArray(string));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static List<Alarm> getAlarmList(JSONArray jArr) {
        List<Alarm> list = new ArrayList<>();
        try {
            for (int i = 0; i < jArr.length(); i++) {
                list.add(new Alarm(jArr.optJSONObject(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
   public static List<Alarm> getDbAlarmList(JSONArray jArr) {
        List<Alarm> list = new ArrayList<>();
        try {
            for (int i = 0; i < jArr.length(); i++) {
                JSONObject j = jArr.optJSONObject(i);
                list.add(new Alarm(j.optString("id"),j.optString("date"),j.optString("message"),j.optString("type"),j.optString("month"),(j.optString("flag").equals("1"))));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Alarm(JSONObject jObj) {
        this.id = jObj.optString("id");
        this.date = jObj.optString("date");
        JSONObject jsonObject = jObj.optJSONObject("value");
        this.message = jsonObject.optString("message");
        this.type = jsonObject.optString("type");
        this.month = jsonObject.optString("month");
        this.flag = false;
    }
}
