package com.mantraideas.irdnepal.models;

import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rowsun on 5/23/16.
 */
public class Notice {
    public String NoticeId, Heading, DatedOn, Url, PublishedFromDate, PublishedToDate;
    public boolean isDownloaded;

    public Notice(String noticeId, String heading, String datedOn, String url, String publishedFromDate, String publishedToDate, Boolean isDownloaded) {
        NoticeId = noticeId;
        Heading = heading;
        DatedOn = datedOn;
        Url = url;
        PublishedFromDate = publishedFromDate;
        PublishedToDate = publishedToDate;
        this.isDownloaded = isDownloaded;
    }

    public static List<Notice> getNoticeList(String string) {
        try {
            return getNoticeList(new JSONArray(string));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static List<Notice> getNoticeList(JSONArray jArr) {
        List<Notice> list = new ArrayList<>();
        try {
            for (int i = 0; i < jArr.length(); i++) {
                list.add(new Notice(jArr.optJSONObject(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Notice(JSONObject jObj) {
        this.NoticeId = jObj.optString("NoticeId");
        this.Heading = jObj.optString("Heading");
        this.DatedOn = jObj.optString("DatedOn");
        this.Url = jObj.optString("Url");
        this.PublishedFromDate = jObj.optString("PublishedFromDate");
        this.PublishedToDate = jObj.optString("PublishedToDate");
        this.isDownloaded = Utils.checkFileExist(this.Url);
    }
}
