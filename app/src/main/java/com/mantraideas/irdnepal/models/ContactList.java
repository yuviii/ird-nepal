package com.mantraideas.irdnepal.models;

import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rowsun on 5/27/16.
 */
public class ContactList {

   public String ContactListId,ContactGroupId,Name,Address,fax,Email,IsVisible,Latitude,Longitude,HitCount,ContactGroup,PhoneNumber;

    public ContactList(String contactListId, String contactGroupId, String name, String address, String fax, String email, String isVisible, String latitude, String longitude, String hitCount, String contactGroup, String phoneNumber) {
        ContactListId = contactListId;
        ContactGroupId = contactGroupId;
        Name = name;
        Address = address;
        this.fax = fax;
        Email = email;
        IsVisible = isVisible;
        Latitude = latitude;
        Longitude = longitude;
        HitCount = hitCount;
        ContactGroup = contactGroup;
        PhoneNumber = phoneNumber;
    }

    public static List<ContactList> getContactList(String string) {
        try {
            return getContactList(new JSONArray(string));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static List<ContactList> getContactList(JSONArray jArr) {
        List<ContactList> list = new ArrayList<>();
        try {
            for (int i = 0; i < jArr.length(); i++) {
                list.add(new ContactList(jArr.optJSONObject(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public ContactList(JSONObject jObj) {
        this.ContactListId = jObj.optString("ContactListId");
        this.ContactGroupId = jObj.optString("ContactGroupId");
        this.Name = jObj.optString("Name");
        this.Address = jObj.optString("Address");
        this.fax = jObj.optString("fax");
        this.Email = jObj.optString("Email");
        this.IsVisible = jObj.optString("IsVisible");
        this.Latitude = jObj.optString("Latitude");
        this.Longitude = jObj.optString("Longitude");
        this.HitCount = jObj.optString("HitCount");
        this.ContactGroup = jObj.optString("ContactGroup");
        try {
            JSONArray jsonArray = jObj.getJSONArray("PhoneNumber");
            JSONObject object = jsonArray.optJSONObject(0);
            this.PhoneNumber = object.optString("Number");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
