package com.mantraideas.irdnepal.models;

import java.util.List;

/**
 * Created by rowsun on 5/27/16.
 */
public class Generic {
    public String data;
    public int type;

    public Generic(String data, int type) {
        this.data = data;
        this.type = type;
    }
}
