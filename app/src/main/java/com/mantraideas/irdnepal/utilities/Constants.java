package com.mantraideas.irdnepal.utilities;

/**
 * Created by rowsun on 5/23/16.
 */
public class Constants {
    public static final String BASE_URL_FILE = "https://ird.gov.np/";
    public static final String BASE_URL_NOTICE = "https://ird.gov.np/mobileapp/Notices/";
    public static final String BASE_URL_ATTATCHMENTS = "https://ird.gov.np/mobileapp/Attachments/";
    public static final String BASE_URL_CONTACTLIST = "https://ird.gov.np/mobileapp/ContactList/";
    public static final String BASE_URL_TAXRATE = "https://ird.gov.np/mobileapp/taxrate/";
    public static final String GOOGLE_API_KEY = "AIzaSyBJ0z0MzESZAvggRDfq0xHMUdnSKl6ittk";
    public static final String GOOGLE_PROJECT_ID = "234109043788";
    public static final String[][] CONTENT_ID = new String[][]{
            {"2042", "12", "2045", "2041", "10", "5024", "5025", "11"},
            {"5021", "5023", "5020"},
            {"2043", "2037", "2036", "5124", "2035"}
    };
    public static final String[] ACTS = new String[]{"Education Service Fee",
            "Excise",
            "Finance Act",
            "Health Service Tax",
            "Income Tax",
            "Liquor Act",
            "Revenue Tribunal Act",
            "VAT"
    };
    public static final String[] DIRECTIVES = new String[]{
            "Excise",
            "Income Tax",
            "VAT"
    };
    public static final String[] ID_NOTICE = new String[]{
            "1016", "1015"
    };
    public static final String[] REGULATIONS = new String[]
            {
                    "Education Service Fee",
                    "Excise",
                    "Income Tax",
                    "Revenue Tribunal Act",
                    "VAT"
            };
    public static final String[] ID_PUBLICATIONS = new String[]{
            "2038",
            "1012",
            "1013",
            "1011"
    };

    public static final String[] TAX_RATE_TYPE = new String[]{
            "married", "single", "nibritibharan_married", "Single_Disabled", "clubbed_Disabled"
    };
    public static final String IRD_CLIENT_ID = "M0b!leAppT0!RD";
    public static final String IRD_TOKEN = "!2D2Sqr&c3VOR@TMxMDAzMTA3MDAwTOK3PO===";
    public static final String LOGIN_TAXPAYER = "LoginTaxpayer";
    public static final String LOGIN_TDS = "LoginTDS";
    public static final String LOGIN_OFFICE = "LoginOffice";
    public static final String LOGIN_TYPE = "login_type";
    public static final String RATE = "rate";
    public static final String FROM = "from";
    public static final String TO = "to";
}
