package com.mantraideas.irdnepal.utilities;

/**
 * Created by rowsun on 5/24/16.
 */
public class DBConstants {
    public static final String TABLE_ATTACHMENTS = "attachments";
    public static final String COLUMN_ATTACHMENT_ID = "ContentAttachmentId";
    public static final String COLUMN_CONTENT_ID = "ContentId";
    public static final String COLUMN_ATTATCHMENT_URL = "AttachmentUrl";
    public static final String ID = "_id";
    public static final String COLUMN_LANGUAGE_IN_ATTACHMENT = "LanguageInAttachMent";
    public static final String COLUMN_ATTATCHMENT_COMMENT_LANGUAGE = "AttachmentCommentlanguage";
    public static final String COLUMN_COMMENT1 = "Comment";
    public static final String COLUMN_COMMENT2 = "Comment2";
    public static final String COLUMN_LAST_UPDATE_DATE = "LastUpdateDate";
    public static final String COLUMN_CONTENT = "Content";
    public static final String CREATE_TABLE_ATTATCHMENT = "CREATE TABLE " + TABLE_ATTACHMENTS +
            " (" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
            COLUMN_ATTACHMENT_ID + " TEXT ," +
            COLUMN_CONTENT_ID + " TEXT ," +
            COLUMN_ATTATCHMENT_URL + " TEXT ," +
            COLUMN_LANGUAGE_IN_ATTACHMENT + " TEXT ," +
            COLUMN_ATTATCHMENT_COMMENT_LANGUAGE + " TEXT ," +
            COLUMN_COMMENT1 + " TEXT ," +
            COLUMN_COMMENT2 + " TEXT ," +
            COLUMN_LAST_UPDATE_DATE + " TEXT ," +
            COLUMN_CONTENT + " TEXT "
            + ");";
    public static final String TABLE_CONTACT_LIST = "contactList";
    public static final String COLUMN_CONTACT_LIST_ID = "ContactListId";
    public static final String COLUMN_CONTACT_GROUP_ID = "ContactGroupId";
    public static final String COLUMN_NAME = "Name";
    public static final String COLUMN_ADDRESS = "Address";
    public static final String COLUMN_FAX = "fax";
    public static final String COLUMN_EMAIL = "Email";
    public static final String COLUMN_IS_VISIBLE = "IsVisible";
    public static final String COLUMN_LATITUDE = "Latitude";
    public static final String COLUMN_LONGITUDE = "Longitude";
    public static final String COLUMN_HIT_COUNT = "HitCount";
    public static final String COLUMN_CONTACT_GROUP = "ContactGroup";
    public static final String COLUMN_PHONE_NUMBER = "PhoneNumber";

    public static final String CREATE_TABLE_CONTACT_LIST = "CREATE TABLE " + TABLE_CONTACT_LIST +
            " (" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
            COLUMN_CONTACT_LIST_ID + " TEXT ," +
            COLUMN_CONTACT_GROUP_ID + " TEXT ," +
            COLUMN_NAME + " TEXT ," +
            COLUMN_ADDRESS + " TEXT ," +
            COLUMN_FAX + " TEXT ," +
            COLUMN_EMAIL + " TEXT ," +
            COLUMN_IS_VISIBLE + " TEXT ," +
            COLUMN_LATITUDE + " TEXT ," +
            COLUMN_LONGITUDE + " TEXT ," +
            COLUMN_HIT_COUNT + " TEXT ," +
            COLUMN_CONTACT_GROUP + " TEXT ," +
            COLUMN_PHONE_NUMBER + " TEXT "
            + ");";

    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_MESSAGE = "message";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_MONTH  = "month";
    public static final String COLUMN_ID = "id";
    public static final String TABLE_ALARM = "alarm";
    public static final String COLUMN_FLAG ="flag" ;
    public static final String CREATE_TABLE_ALARM = "CREATE TABLE " + TABLE_ALARM +
            " (" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
            COLUMN_ID + " TEXT ," +
            COLUMN_DATE + " TEXT ," +
            COLUMN_MESSAGE + " TEXT ," +
            COLUMN_TYPE+ " TEXT ," +
            COLUMN_MONTH+ " TEXT ," +
            COLUMN_FLAG + " TEXT "
            + ");";
    public static final String COLUMN_VALUE = "value";
    public static final String TABLE_TAX_RATE = "taxrate";
    public static final String COLUMN_YEAR = "year";
    public static final String CREATE_TABLE_TAX_RATE = "CREATE TABLE "+ TABLE_TAX_RATE +
            " (" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
            COLUMN_YEAR + " TEXT ," +
            COLUMN_VALUE + " TEXT "
            + " );";



}
