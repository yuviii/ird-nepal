package com.mantraideas.irdnepal.utilities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.support.design.BuildConfig;
import android.support.design.widget.Snackbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by rowsun on 5/23/16.
 */
public class Utils {
    public static final String SD_PATH = Environment.getExternalStorageDirectory() + "/IRDNepal";
    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    public static void snack(View view, String message){
        Snackbar.make(view,message,Snackbar.LENGTH_SHORT).show();
    }
    public static void log(String message) {
         Log.d("Rowsun", message);
    }
    public static String getRelativeTime(long timestamp) {
        long nowtime = System.currentTimeMillis();
        if(timestamp < nowtime){
            return (String) DateUtils.getRelativeTimeSpanString(timestamp, nowtime, 1000);
        }
        return "Just now";
    }
    public static boolean isNetworkConnected(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo()!=null;
    }
    public static String getFileNameFromUrl(String url) {
        try {
            return url.substring(url.lastIndexOf("/") + 1);
        } catch (Exception e) {
        }
        return "";
    }
    public static boolean contains(JSONObject jsonObject, String key) {
        return jsonObject != null && jsonObject.has(key) && !jsonObject.isNull(key) ? true : false;
    }

    public static File getSDPathFromUrl(String url) {
        return createFilePath(SD_PATH, URLDecoder.decode(getFileNameFromUrl(url)));
    }

    public static boolean checkFileExist(String url) {
        if(url == null || url.isEmpty())
            return false;
        File f = createFilePath(SD_PATH,URLDecoder.decode(getFileNameFromUrl(url)));
        return f.exists();
    }

    private static File createFilePath(String folder, String filename) {
        File f = new File(folder);
        if (!f.exists())
            f.mkdirs();
        return new File(f, filename);
    }

    public static String getUrl(String url) {
        try {
            String query = URLEncoder.encode(getFileNameFromUrl(url), "utf-8");
            if (!url.startsWith("http")) {
                url =  Constants.BASE_URL_FILE + url;
            }
                return  url.substring(0, url.lastIndexOf("/")) + "/" + query.replaceAll("%25", "%").replaceAll("\\+","%20");

        }catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }

    public static String getVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void doRate(Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("market://details?id=" + context.getPackageName()))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("http://play.google.com/store/apps/details?id="
                            + context.getPackageName()))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }

    }
}
