package com.mantraideas.irdnepal.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mantraideas.irdnepal.R;

public class EstimatedReturnsFragment extends Fragment {


    public EstimatedReturnsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_estimated_returns, container, false);
    }

}
