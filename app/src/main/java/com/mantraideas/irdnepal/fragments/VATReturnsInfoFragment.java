package com.mantraideas.irdnepal.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mantraideas.irdnepal.R;

public class VATReturnsInfoFragment extends Fragment {


    public VATReturnsInfoFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vatreturns_info, container, false);
    }

}
