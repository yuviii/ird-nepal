package com.mantraideas.irdnepal.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.soap.IIntegratedTaxSystemService;
import com.mantraideas.irdnepal.soap.IServiceEvents;
import com.mantraideas.irdnepal.soap.OperationResult;

/**
 * Created by rowsun on 6/21/16.
 */
public class RegistrationDetailFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_registration_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        IIntegratedTaxSystemService client = new IIntegratedTaxSystemService(new IServiceEvents() {
            @Override
            public void Starting() {
            }

            @Override
            public void Completed(OperationResult result) {

            }
        });
    }

}
