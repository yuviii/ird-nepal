package com.mantraideas.irdnepal.helpers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.WakefulBroadcastReceiver;


import com.mantraideas.irdnepal.utilities.Utils;

import java.util.Calendar;

import okhttp3.internal.Util;

public class AlarmReceiver extends WakefulBroadcastReceiver {
    private static AlarmManager alarmMgr;
    private static PendingIntent alarmIntent;


    @Override
    public void onReceive(Context context, Intent intent) {
        Utils.log("Alarm Receiver on Receive.." );
        Intent service = new Intent(context, SchedulingService.class);

        if (intent.hasExtra("message")) {
            service.putExtra("message", intent.getStringExtra("message"));
        }
        startWakefulService(context, service);
        Utils.log("Alarm Receiver, Start wakeful service");
    }

    public static void setAlarm(Context context, Calendar calendar, String message) {
        alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("message", message);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
        Utils.log("Alarm Receiver Alarm Set");
    }

    public static void cancelAlarm(Context context) {
        Utils.log("Cancel Alarm");
        if (alarmMgr != null) {
            alarmMgr.cancel(alarmIntent);
        }
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
