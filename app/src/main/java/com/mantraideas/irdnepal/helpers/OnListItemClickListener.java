package com.mantraideas.irdnepal.helpers;

/**
 * Created by rowsun on 5/25/16.
 */
public interface OnListItemClickListener {
    void OnItemClicked(String id);
}
