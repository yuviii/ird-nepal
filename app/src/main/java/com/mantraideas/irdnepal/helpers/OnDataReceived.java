package com.mantraideas.irdnepal.helpers;

/**
 * Created by rowsun on 5/23/16.
 */
public interface OnDataReceived {
    void onSuccess(String table_name, String result);
    void onError(String table_name, int status, String message);
}
