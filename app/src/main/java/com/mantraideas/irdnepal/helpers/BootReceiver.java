package com.mantraideas.irdnepal.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mantraideas.irdnepal.models.Alarm;
import com.mantraideas.irdnepal.utilities.Utils;

/**
 * Created by rowsun on 7/5/16.
 */
public class BootReceiver extends BroadcastReceiver {
    AlarmManagerUtils alarmManagerUtils;
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
           // Utils.log("Boot Completed");
            alarmManagerUtils = new AlarmManagerUtils(context);
            alarmManagerUtils.initializedAlarm();
        }
    }
}
