package com.mantraideas.irdnepal.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mantraideas.irdnepal.models.Alarm;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import okhttp3.internal.Util;

/**
 * Created by rowsun on 7/6/16.
 */
public class CancelNotification extends BroadcastReceiver {
    DatabaseHelper db;
    Preference pf;
    @Override
    public void onReceive(Context context, Intent intent) {
        db = new DatabaseHelper(context);
        pf = new Preference(context);
        if(SchedulingService.mNotificationManager!=null) {
            SchedulingService.mNotificationManager.cancel(1);
        }
        Utils.log("Cancel notification");
        Alarm a = db.getAlarm(pf.getPreference("current_alarm"));
        db.updateFlag(a.month,a.type);
        AlarmManagerUtils.alarmDetailsList.addAll(Alarm.getDbAlarmList(db.getDataInJSON(DBConstants.TABLE_ALARM)));
        new AlarmManagerUtils(context).initializedAlarm();
        // load data from database where flag 0
        // create list
        // initialize notification
    }
}
