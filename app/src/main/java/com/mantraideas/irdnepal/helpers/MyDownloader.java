package com.mantraideas.irdnepal.helpers;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.widget.Toast;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.activities.MainActivity;
import com.mantraideas.irdnepal.utilities.Utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by rowsun on 5/23/16.
 */
public class MyDownloader extends AsyncTask<Void, Integer, String> {
    String link;
    Context context;
    Boolean running = true;
    int index;
    File file;
    int id = 1;
    onDownloadCompleteListner action;

    public MyDownloader(Context context, String link) {
        this.link = link;
        this.context = context;
    }

    public MyDownloader(Context context, int index, String link,
                        onDownloadCompleteListner action) {
        this.link = link;
        this.context = context;
        this.index = index;
        this.action = action;
    }

    public static interface onDownloadCompleteListner {
        public void downloadListener(int index, String link, boolean completed);
    }

    @Override
    protected String doInBackground(Void... params) {
        int count;
        try {
            URL links = new URL(link);
            URLConnection conection = links.openConnection();
            conection.connect();
            int lenghtOfFile = conection.getContentLength();
            Utils.log("File size = " + lenghtOfFile);
            if(lenghtOfFile <= 0){
                return "";
            }
            InputStream input = new BufferedInputStream(links.openStream(),
                    8 * 1024);
            file = Utils.getSDPathFromUrl(link);
            OutputStream output = new FileOutputStream(file);
            byte data[] = new byte[8 * 1024];
            int total = 0;
            int lastProgress = 0;
            if (running) {
                while ((count = input.read(data)) != -1) {
                    total += count;
                    output.write(data, 0, count);
                    int progress = ((total * 100) / lenghtOfFile);
                    if (lastProgress < progress) {
                        lastProgress = progress;
                        publishProgress(progress);
                    }
                }
            }
            output.flush();
            output.close();
            input.close();
            return file.getPath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        initializeProgressBar();
        initializeProgressBox();
        mBuilder.setProgress(100, 0, false);
        notificationManager.notify(id, mBuilder.build());

    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (!running && file.exists()) {
            file.delete();
        }
        if(TextUtils.isEmpty(result)){
            if( Utils.getSDPathFromUrl(link).exists()){
                Utils.getSDPathFromUrl(link).delete();
            }
            updateProgressMessage("File not found in server");
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("File not found in server").setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
        else {
            updateProgressMessage("Download complete");
            showAlert();
        }

    }
    private void updateProgressMessage(String message){
        mBuilder.setContentText(message).setProgress(0, 0, false);
        // if(new Pref(context).getBoolPreferences(Pref.KEY_SOUND)) {
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        //}
        notificationManager.notify(id, mBuilder.build());
        prgbar.dismiss();

        if (action != null) {
            action.downloadListener(index, link, running);
        }

    }
//
//    private void showAlert() {
//        if (context == null) {
//            return;
//        }
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setTitle("Download Completed")
//                .setMessage("Do you want to open now ?")
//                .setPositiveButton("Read Now",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,
//                                                int which) {
//                                readPdfFile(context, file);
//                            }
//
//                        }).setNegativeButton("Read Later", null).create()
//                .show();
//
//    }

    public void showAlert() {
        File file = Utils.getSDPathFromUrl(link);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if(link.contains("pdf")) {
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        }
        else if(link.contains("mp3")){
            intent.setDataAndType(Uri.fromFile(file), "audio/*");
        }
        else if (link.contains(".jpg") || link.contains(".jpeg") || link.contains(".png")) {
            intent.setDataAndType(Uri.fromFile(file), "image/*");
        }
        else if (link.contains(".3gp") || link.contains(".mpg") || link.contains(".mpeg") || link.contains(".mpe") || link.contains(".mp4") || link.contains(".avi")) {
            intent.setDataAndType(Uri.fromFile(file), "video*/*");
        }
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Download Complete, Open With ..");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent openInent = Intent.createChooser(intent, context.getString(R.string.app_name));
        try {
            context.startActivity(openInent);

        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No File reader found. please download the reader from playstore", Toast.LENGTH_SHORT).show();
        }
//        return null;
    }

    public static void readPdfFile(final Context context, File file) {
        if (file == null) {
            return;
        }
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setDataAndType(Uri.fromFile(file), "application/pdf");
            context.startActivity(i);

        } catch (ActivityNotFoundException e) {
            Toast.makeText(context,
                    "No application available to View this type of file. :(",
                    Toast.LENGTH_LONG).show();
            new AlertDialog.Builder(context).setTitle("PDF reader not installed. Download now?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                            .parse("market://details?id=com.adobe.reader"))
                            );
                }
            }).setNegativeButton("Cancel",null).show();

//            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
//                    .parse("market://details?id=" + context.getPackageName()))
//                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    android.support.v4.app.NotificationCompat.Builder mBuilder;
    NotificationManager notificationManager;
    Uri sound;

    private void initializeProgressBar() {

        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentTitle(context.getString(R.string.app_name));
        mBuilder.setSmallIcon(R.drawable.ic_sil);
        notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        sound = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (sound == null) {
            sound = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        }
        Intent intent = new Intent(context, MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
    }

    ProgressDialog prgbar;

    private void initializeProgressBox() {

        prgbar = new ProgressDialog(context);
        prgbar.setIcon(R.mipmap.ic_launcher);
        prgbar.setTitle("Downloading ...");
        prgbar.setMessage("Please wait");
        prgbar.setMax(100);
        prgbar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        prgbar.setButton(DialogInterface.BUTTON_POSITIVE, "Hide",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        prgbar.dismiss();

                    }
                });
        prgbar.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (AsyncTask.Status.RUNNING != null) {
                            cancel(true);
                            running = false;
                        }
                        mBuilder.setContentText(" Oops!!! Download cancelled")
                                .setProgress(0, 0, false);
                        notificationManager.notify(id, mBuilder.build());
                    }
                });
        prgbar.show();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        mBuilder.setProgress(100, values[0], false);
        mBuilder.setContentText(values[0] + "% complete please wait...");
        prgbar.setProgress(values[0]);

        notificationManager.notify(id, mBuilder.build());

        super.onProgressUpdate(values);

    }

}
