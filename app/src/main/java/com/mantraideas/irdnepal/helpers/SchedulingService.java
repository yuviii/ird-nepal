package com.mantraideas.irdnepal.helpers;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.RemoteViews;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.activities.MainActivity;
import com.mantraideas.irdnepal.utilities.Utils;

/**
 * Created by rowsun on 7/5/16.
 */
public class SchedulingService extends IntentService {

    public static final int NOTIFICATION_ID = 1;
    public static NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    AlarmManagerUtils alarmManagerUtils;

    public SchedulingService() {
        super("service");

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Utils.log("Scheduling Service, On Handle Intent. Intent  = " + intent.getStringExtra("message"));
        if (intent.getStringExtra("message")!=null) {
            sendNotification(intent.getStringExtra("message"));
        }
        AlarmReceiver.cancelAlarm(this);
        alarmManagerUtils = new AlarmManagerUtils(this);
        alarmManagerUtils.initializedAlarm();
    }



    private void sendNotification(String msg) {
        Intent deleteIntent = new Intent(this, CancelNotification.class);
        Intent cancelIntent = new Intent(this,DismissNotification.class);
        PendingIntent pendingIntentCancel = PendingIntent.getBroadcast(this, 0, deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingIntentDismiss = PendingIntent.getBroadcast(this, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentTitle(getString(R.string.app_name))
                        .addAction(R.drawable.ic_done_all_white_36dp,"Already submitted",pendingIntentCancel)
                        .addAction(R.drawable.ic_cancel_white_36dp,"Submit later",pendingIntentDismiss)
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
