package com.mantraideas.irdnepal.helpers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.activities.MainActivity;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by rowsun on 5/31/16.
 */
public class FirebaseMessanging extends FirebaseMessagingService {
    private static final String TAG = "IRD";
    private Preference pf;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Map<String, String> a = remoteMessage.getData();
        JSONObject j = new JSONObject(a);
        generateNotification(getApplicationContext(), remoteMessage.getNotification().getBody(), a.get("message"), a.get("data"), a.get("type"));
        pf = new Preference(this);
        pf.setPreference("notification", j.toString());
    }


    private void generateNotification(Context context, String name, String message, String data, String type) {

        NotificationCompat.Builder mBuilder;
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent;
        if (data == null || data.length() == 0) {
            notificationIntent = new Intent(context, MainActivity.class);
            notificationIntent.putExtra("page", type);
        } else {
            notificationIntent = new Intent(Intent.ACTION_VIEW);
            notificationIntent.setData(Uri.parse(data));
        }
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentTitle(name)
                .setSmallIcon(R.drawable.ic_sil)
                .setContentIntent(pendingIntent)
                .setContentText(message)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message));

        notificationManager.notify((int) (Math.random() * 10000), mBuilder.build());

    }
}

