package com.mantraideas.irdnepal.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mantraideas.irdnepal.models.Alarm;
import com.mantraideas.irdnepal.models.Attachments;
import com.mantraideas.irdnepal.models.ContactList;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rowsun on 5/24/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private Context mContext;
    private static final String DB_NAME = "irdnepal";
    private static final int DB_VERSION = 2;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(DBConstants.CREATE_TABLE_ATTATCHMENT);
            db.execSQL(DBConstants.CREATE_TABLE_CONTACT_LIST);
            db.execSQL(DBConstants.CREATE_TABLE_ALARM);
            db.execSQL(DBConstants.CREATE_TABLE_TAX_RATE);
            Utils.log("Table Created.......................");
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP " + DBConstants.TABLE_ATTACHMENTS + " IF EXISTS;");
            db.execSQL("DROP " + DBConstants.TABLE_CONTACT_LIST + " IF EXISTS;");
            db.execSQL("DROP " + DBConstants.TABLE_ALARM + " IF EXISTS;");
            db.execSQL("DROP " + DBConstants.TABLE_TAX_RATE + " IF EXISTS;");
            onCreate(db);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public boolean addTaxRate(JSONObject jsonObject) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.COLUMN_YEAR, jsonObject.optString(DBConstants.COLUMN_YEAR));
        values.put(DBConstants.COLUMN_VALUE, jsonObject.optString(DBConstants.COLUMN_VALUE));
        SQLiteDatabase db = getWritableDatabase();
        db.insert(DBConstants.TABLE_TAX_RATE, null, values);
        db.close();
        return true;
    }

    public boolean addAttachment(JSONObject jObj) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.COLUMN_ATTACHMENT_ID, jObj.optString(DBConstants.COLUMN_ATTACHMENT_ID));
        values.put(DBConstants.COLUMN_CONTENT_ID, jObj.optString(DBConstants.COLUMN_CONTENT_ID));
        values.put(DBConstants.COLUMN_ATTATCHMENT_URL, jObj.optString(DBConstants.COLUMN_ATTATCHMENT_URL));
        values.put(DBConstants.COLUMN_LANGUAGE_IN_ATTACHMENT, jObj.optString(DBConstants.COLUMN_LANGUAGE_IN_ATTACHMENT));
        values.put(DBConstants.COLUMN_ATTATCHMENT_COMMENT_LANGUAGE, jObj.optString(DBConstants.COLUMN_ATTATCHMENT_COMMENT_LANGUAGE));
        values.put(DBConstants.COLUMN_COMMENT1, jObj.optString(DBConstants.COLUMN_COMMENT1));
        values.put(DBConstants.COLUMN_COMMENT2, jObj.optString(DBConstants.COLUMN_COMMENT2));
        values.put(DBConstants.COLUMN_LAST_UPDATE_DATE, jObj.optString(DBConstants.COLUMN_LAST_UPDATE_DATE));
        values.put(DBConstants.COLUMN_CONTENT, jObj.optString(DBConstants.COLUMN_CONTENT));
        SQLiteDatabase db = getWritableDatabase();
        db.insert(DBConstants.TABLE_ATTACHMENTS, null, values);
        db.close();
        return true;
    }

    public boolean addAlarm(Alarm alarm) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.COLUMN_ID, alarm.id);
        values.put(DBConstants.COLUMN_DATE, alarm.date);
        values.put(DBConstants.COLUMN_MESSAGE, alarm.message);
        values.put(DBConstants.COLUMN_TYPE, alarm.type);
        values.put(DBConstants.COLUMN_MONTH, alarm.month);
        values.put(DBConstants.COLUMN_FLAG, alarm.flag ? 1 : 0);
        SQLiteDatabase db = getWritableDatabase();
        db.insert(DBConstants.TABLE_ALARM, null, values);
        db.close();
        return true;
    }

    public boolean updateAlarm(Alarm alarm) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBConstants.COLUMN_ID, alarm.id);
        values.put(DBConstants.COLUMN_MESSAGE, alarm.message);
        values.put(DBConstants.COLUMN_TYPE, alarm.type);
        values.put(DBConstants.COLUMN_MONTH, alarm.month);
        values.put(DBConstants.COLUMN_FLAG, alarm.flag ? "1" : "0");
        db.update(DBConstants.TABLE_ALARM, values, DBConstants.COLUMN_ID + "=?", new String[]{alarm.id});
        db.close();
        return false;
    }


    public boolean addContactList(JSONObject jObj) {
        ContentValues values = new ContentValues();
        values.put(DBConstants.COLUMN_CONTACT_LIST_ID, jObj.optString(DBConstants.COLUMN_CONTACT_LIST_ID));
        values.put(DBConstants.COLUMN_CONTACT_GROUP_ID, jObj.optString(DBConstants.COLUMN_CONTACT_GROUP_ID));
        values.put(DBConstants.COLUMN_NAME, jObj.optString(DBConstants.COLUMN_NAME));
        values.put(DBConstants.COLUMN_ADDRESS, jObj.optString(DBConstants.COLUMN_ADDRESS));
        values.put(DBConstants.COLUMN_FAX, jObj.optString(DBConstants.COLUMN_FAX));
        values.put(DBConstants.COLUMN_EMAIL, jObj.optString(DBConstants.COLUMN_EMAIL));
        values.put(DBConstants.COLUMN_IS_VISIBLE, jObj.optString(DBConstants.COLUMN_IS_VISIBLE));
        values.put(DBConstants.COLUMN_LATITUDE, jObj.optString(DBConstants.COLUMN_LATITUDE));
        values.put(DBConstants.COLUMN_LONGITUDE, jObj.optString(DBConstants.COLUMN_LONGITUDE));
        values.put(DBConstants.COLUMN_HIT_COUNT, jObj.optString(DBConstants.COLUMN_HIT_COUNT));
        values.put(DBConstants.COLUMN_CONTACT_GROUP, jObj.optString(DBConstants.COLUMN_CONTACT_GROUP));
        try {
            JSONArray jsonArray = jObj.getJSONArray("PhoneNumber");
            JSONObject object = jsonArray.optJSONObject(0);
            values.put(DBConstants.COLUMN_PHONE_NUMBER, object.optString("Number"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SQLiteDatabase db = getWritableDatabase();
        db.insert(DBConstants.TABLE_CONTACT_LIST, null, values);
        Utils.log("Data Successfully inserted");
        db.close();
        return true;
    }

    public boolean updateData(JSONObject jObj) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(DBConstants.COLUMN_ATTACHMENT_ID, jObj.optString(DBConstants.COLUMN_ATTACHMENT_ID));
            values.put(DBConstants.COLUMN_CONTENT_ID, jObj.optString(DBConstants.COLUMN_CONTENT_ID));
            values.put(DBConstants.COLUMN_ATTATCHMENT_URL, jObj.optString(DBConstants.COLUMN_ATTATCHMENT_URL));
            values.put(DBConstants.COLUMN_LANGUAGE_IN_ATTACHMENT, jObj.optString(DBConstants.COLUMN_LANGUAGE_IN_ATTACHMENT));
            values.put(DBConstants.COLUMN_ATTATCHMENT_COMMENT_LANGUAGE, jObj.optString(DBConstants.COLUMN_ATTATCHMENT_COMMENT_LANGUAGE));
            values.put(DBConstants.COLUMN_COMMENT1, jObj.optString(DBConstants.COLUMN_COMMENT1));
            values.put(DBConstants.COLUMN_COMMENT2, jObj.optString(DBConstants.COLUMN_COMMENT2));
            values.put(DBConstants.COLUMN_LAST_UPDATE_DATE, jObj.optString(DBConstants.COLUMN_LAST_UPDATE_DATE));
            values.put(DBConstants.COLUMN_CONTENT, jObj.optString(DBConstants.COLUMN_CONTENT));

            return (db.update(DBConstants.TABLE_ATTACHMENTS, values, DBConstants.COLUMN_ATTACHMENT_ID + "=?", new String[]{jObj.getString(DBConstants.COLUMN_ATTACHMENT_ID)})) == 1;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        db.close();
        return false;
    }

    public JSONArray getData(String table_name, String key, String value, int skip, int limit) {
        String sql = "SELECT * FROM " + table_name;
        if (key != null) {
            sql += " WHERE " + key + "=\"" + value + "\"";
        }
        sql += " ORDER BY id DESC";
        if (limit > 0) {
            sql += " LIMIT " + skip + ", " + limit;
        }
        return queryData(sql);
    }

    public JSONArray queryData(String sql) {
        SQLiteDatabase db = this.getReadableDatabase();
        JSONArray retJSONArr = getJSONFromCursor(db.rawQuery(sql, null));
        db.close();
        return retJSONArr;
    }

    private JSONArray getJSONFromCursor(Cursor cursor) {
        JSONArray retJSONArr = new JSONArray();
        if (cursor.moveToFirst()) {
            do {
                JSONObject jObj = new JSONObject();
                String columnName[] = cursor.getColumnNames();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    try {
                        String fkey = columnName[i];
                        String fval = cursor.getString(i);
                        jObj.put(fkey, fval);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                retJSONArr.put(jObj);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return retJSONArr;
    }

    public JSONArray getData(String table_name) {
        return queryData("SELECT * FROM " + table_name + " ORDER BY id DESC");
    }

    public JSONObject getDataFromId(String table_name, String id) {
        JSONArray jArr = queryData("SELECT * FROM " + table_name + " WHERE id=\"" + id + "\"");
        if (jArr.length() > 0) {
            jArr.optJSONObject(0);
        }
        return null;
    }

    public JSONArray getDataInJSON(String tableName) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        cursor = db.query(tableName, null, null, null, null, null, null);
        JSONArray jsonArray = new JSONArray();
        boolean dataExist = cursor.moveToFirst();
        if (dataExist) {
            Utils.log("Data Exists");
            int count = cursor.getColumnCount();
            try {

                Utils.log("Data =" + count);
                do {
                    JSONObject jsonObject = new JSONObject();
                    for (int i = 0; i < count; i++) {
                        jsonObject.put(cursor.getColumnName(i), cursor.getString(i));
                    }
                    jsonArray.put(jsonObject);
                    Utils.log("obj from db" + jsonObject.toString());
                } while (cursor.moveToNext());
                return jsonArray;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        cursor.close();
        db.close();
        return jsonArray;
    }


    public void updateFlag(String month, String type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBConstants.COLUMN_FLAG, "1");
        db.update(DBConstants.TABLE_ALARM, values, DBConstants.COLUMN_MONTH + " = ? AND " + DBConstants.COLUMN_TYPE + " = ? ", new String[]{month, type});
        Utils.log("Update flag, type = " + type + "month = " + month);
        db.close();
    }

    public List<Alarm> getAlarmList() {
        List<Alarm> alarms = new ArrayList<>();
        String sql = "SELECT * FROM " + DBConstants.TABLE_ALARM + "WHERE " + DBConstants.COLUMN_FLAG + " = '0'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                Alarm a = new Alarm(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6).equals("1"));
                alarms.add(a);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return alarms;
    }

    public List<Attachments> getAttachments(String contentId, String lanId) {
        List<Attachments> result = new ArrayList<>();
        if (contentId != null) {

            String sql = "SELECT * FROM " + DBConstants.TABLE_ATTACHMENTS;
            sql += " WHERE " + DBConstants.COLUMN_CONTENT_ID + "= '" + contentId + "'";
            if (lanId != null) {
                sql += " AND " + DBConstants.COLUMN_LANGUAGE_IN_ATTACHMENT + "= '" + lanId + "'";
            }
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                do {
                    Attachments a = new Attachments(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9));
                    result.add(a);
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();

        }

        return result;
    }

    public Alarm getAlarm(String id) {
        Alarm a = new Alarm();
        String sql = "SELECT * FROM " + DBConstants.TABLE_ALARM + " WHERE id= " + id;
        Utils.log(sql);
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            {
                a = new Alarm(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6).equals("1"));
            }
        }
        cursor.close();
        db.close();
        return a;

    }

    public Attachments getAttachment(String id) {
        Attachments s = new Attachments();
        String sql = "SELECT * FROM " + DBConstants.TABLE_ATTACHMENTS + " WHERE id=" + id;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            {
                Attachments a = new Attachments(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9));
            }
        }
        cursor.close();
        db.close();
        return s;
    }

    public List<ContactList> getContactList() {
        List<ContactList> result = new ArrayList<>();

        String sql = "SELECT * FROM " + DBConstants.TABLE_CONTACT_LIST + " ORDER BY " + DBConstants.COLUMN_CONTACT_GROUP_ID;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                ContactList a = new ContactList(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11), cursor.getString(12));
                result.add(a);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return result;
    }

    public boolean isExists(String table, String id) {
        String sql = "Select * from " + table + " where " + DBConstants.COLUMN_ATTACHMENT_ID + "=" + id;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            return true;
        }
        cursor.close();
        db.close();
        return false;

    }

    public boolean isDataExists(String table) {
        String sql = "Select * from " + table;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            return true;
        }
        cursor.close();
        db.close();
        return false;

    }

    public boolean isContactListExists(String table, String id) {
        String sql = "Select * from " + table + " where " + DBConstants.COLUMN_CONTACT_LIST_ID + "='" + id + "'";
        Utils.log(sql);
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            Utils.log("Data exists");
            return true;
        }
        cursor.close();
        db.close();
        return false;

    }

    public void deleteAllData(String table) {
        String sql = "DELETE FROM " + table + " ";
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(sql);
        db.close();
    }

}
