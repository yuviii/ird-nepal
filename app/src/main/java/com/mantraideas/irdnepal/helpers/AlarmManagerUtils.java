package com.mantraideas.irdnepal.helpers;

import android.content.Context;
import android.os.SystemClock;


import com.mantraideas.irdnepal.models.Alarm;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


public class AlarmManagerUtils {
    Context context;
//    AlarmReceiver alarmReceiver;
    DatabaseHelper db;
   static List<Alarm> alarmDetailsList;

    public AlarmManagerUtils(Context context) {
        this.context = context;
//        this.alarmReceiver = new AlarmReceiver();
        db = new DatabaseHelper(context);
        alarmDetailsList = new ArrayList<>();
        alarmDetailsList.addAll(Alarm.getDbAlarmList(db.getDataInJSON(DBConstants.TABLE_ALARM)));
        Utils.log(alarmDetailsList.size() + " ");
    }

    public void initializedAlarm() {
        Utils.log("Alarm Initialized");
        if (alarmDetailsList == null) {
            Utils.log("Alarm list empty");
            return;
        } else {
            for (int i = 0; i < alarmDetailsList.size(); i++) {
                Alarm a = alarmDetailsList.get(i);
                Date m = getTimeFromString(a.date);
                Utils.log("Date = " + m);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(m);
                calendar.set(Calendar.HOUR_OF_DAY,8);
                Utils.log("Message = " + a.message + " Date = " + a.date);
                Utils.log("Calender Time" + calendar);
                Utils.log("Current Time " + Calendar.getInstance());
                if (Calendar.getInstance().before(calendar)) {
                    if (!a.flag) {
                        setAlarm(a, false);
                        new Preference(context).setPreference("current_alarm",a.id);
//                        a.flag = true;
//                        db.updateAlarm(a);
                        break;
                    }
                }
                if (i == alarmDetailsList.size()) {
                    setAlarm(alarmDetailsList.get(0), true);
                    new Preference(context).setPreference("current_alarm",alarmDetailsList.get(0).id);
                }

                // check the current date with the date in the list
                // compare the date
                // find the alarm time
                // check the flag
                // flag true skip all the date for this month
                //if the list end without finding restart the list , add year with one

            }
        }
    }



    private Date getTimeFromString(String time) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = (Date) formatter.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    private void setAlarm(Alarm details, boolean nextDay) {
        Utils.log("Set alarm");
        Calendar calendar = Calendar.getInstance();
        if (nextDay) {
            calendar.add(Calendar.YEAR, 1);
        }
        Date date = getTimeFromString(details.date);
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 8);
//        calendar.add(Calendar.MONTH,1);
//        calendar.set(Calendar.MONTH,date.getMonth());
//        calendar.set(Calendar.DAY_OF_MONTH,date.getDay());
        Utils.log("Alarm Manager Utils, Next Alarm " + "Month " + calendar+", Message" + details.message);
        AlarmReceiver.setAlarm(context, calendar, details.message);
    }
}


