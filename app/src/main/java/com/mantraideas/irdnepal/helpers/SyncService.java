package com.mantraideas.irdnepal.helpers;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.activities.MainActivity;
import com.mantraideas.irdnepal.models.Attachments;
import com.mantraideas.irdnepal.utilities.Constants;
import com.mantraideas.irdnepal.utilities.DBConstants;
import com.mantraideas.irdnepal.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rowsun on 6/16/16.
 */
public class SyncService extends IntentService implements OnDataReceived {

    public SyncService() {
        super("Sync Service");

    }

    DataQuery dq;
    DatabaseHelper db;
    int index = 0;
    String[] urls;
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;

    @Override
    protected void onHandleIntent(Intent intent) {
        if (!Utils.isNetworkConnected(getApplicationContext())) {
            return;
        }
        Utils.log("Intent Service Started");
        db = new DatabaseHelper(this);
        urls = new String[]{Constants.BASE_URL_ATTATCHMENTS, Constants.BASE_URL_CONTACTLIST, Constants.BASE_URL_TAXRATE};
        dq = new DataQuery(this, this);
        requestData(index);
    }


    private void sendNotification() {
        Intent deleteIntent = new Intent(this, CancelNotification.class);
        PendingIntent pendingIntentCancel = PendingIntent.getBroadcast(this, 0, deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mNotifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(""))
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText("Please wait, Syncing data....");
        mBuilder.setContentIntent(contentIntent);
        mNotifyManager.notify(111, mBuilder.build());
    }

    private void requestData(int index) {
        try {
            dq.getRequestData(urls[index]);
        } catch (Exception e) {
            if (mNotifyManager != null) {
//                mNotifyManager.cancel(111);
            }
        }
    }

    @Override
    public void onSuccess(String table_name, final String result) {
        if (table_name.equals(Constants.BASE_URL_ATTATCHMENTS)) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {

                    try {
                        JSONArray jsonArray = new JSONArray(result);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            if (!db.isExists(DBConstants.TABLE_ATTACHMENTS, object.optString(DBConstants.COLUMN_ATTACHMENT_ID))) {
                                db.addAttachment(object);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;

                }

            }.execute();
        } else if (table_name.equals(Constants.BASE_URL_CONTACTLIST)) {

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            if (!db.isContactListExists(DBConstants.TABLE_CONTACT_LIST, object.optString(DBConstants.COLUMN_CONTACT_LIST_ID))) {
                                db.addContactList(object);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.execute();
        } else if (table_name.equals(Constants.BASE_URL_TAXRATE)) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        db.deleteAllData(DBConstants.TABLE_TAX_RATE);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            db.addTaxRate(object);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.execute();
        }
        index++;
        requestData(index);

    }

    @Override
    public void onError(String table_name, int status, String message) {

    }
}
