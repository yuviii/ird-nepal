package com.mantraideas.irdnepal.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by rowsun on 7/12/16.
 */
public class DismissNotification extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
    if(SchedulingService.mNotificationManager!=null) {
        SchedulingService.mNotificationManager.cancel(1);
    }
    }
}
