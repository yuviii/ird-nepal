package com.mantraideas.irdnepal.helpers;

import android.content.Context;
import android.os.AsyncTask;

import com.mantraideas.irdnepal.utilities.Utils;

import java.util.HashMap;

/**
 * Created by rowsun on 5/23/16.
 */
public class DataQuery {
    Context context;
    OnDataReceived action;
    public DataQuery(Context context, OnDataReceived action){
        this.context = context;
        this.action = action;
    }



    public void getRequestData(String url){
        getRequestData(url,getRequestParameters());
    }

    public void getRequestData(final String table, final HashMap<String, String> params){
        new AsyncTask<Void, Void, String>(){
            @Override
            protected String doInBackground(Void... voids) {
                return new ServerRequest(context).httpPostData(table,params);
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if(action != null){
                    action.onSuccess(table, result);
                }
            }
        }.execute();
    }

    public void getRequestDataUrl(final String url){
        Utils.log("Get Requested data:"+url);
        new AsyncTask<Void, Void, String>(){
            @Override
            protected String doInBackground(Void... voids) {
                return new ServerRequest(context).httpGetData(url);
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if(action != null){
                    action.onSuccess(url, result);
                }
            }
        }.execute();
    }
    public HashMap<String, String> getRequestParameters() {
        HashMap<String, String> params = new HashMap<String, String>();
        return params;
    }
}
