package com.mantraideas.irdnepal.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by rowsun on 5/31/16.
 */
public class Preference {
    SharedPreferences preference;

    public Preference(Context context) {
        preference = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getPreference(String key) {
        return preference.getString(key, "");
    }

    public void setPreference(String key, String value) {
        preference.edit().putString(key, value).apply();
    }

    public void setIntPreference(String key, int value) {
        preference.edit().putInt(key, value).apply();
    }


    public int getIntPreference(String key) {
        return preference.getInt(key, 0);
    }

    public boolean getBoolPreference(String key) {
        return preference.getBoolean(key, false);
    }

    public boolean getBoolPreference(String key, boolean fallback) {
        return preference.getBoolean(key, fallback);
    }

    public void setBoolPreference(String key, boolean value) {
        preference.edit().putBoolean(key, value).apply();
    }

    public void clearAll() {

    }

}