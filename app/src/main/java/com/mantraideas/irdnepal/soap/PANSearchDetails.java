package com.mantraideas.irdnepal.soap;

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.5.7.1
//
// Created by Quasar Development at 01/07/2016
//
//---------------------------------------------------


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class PANSearchDetails extends AttributeContainer implements KvmSerializable
{

    
    public String CityName;
    
    public String ExciseAccountStatus;
    
    public String ExciseRegistrationDate;
    
    public String FilingPeriod;
    
    public String ITAccountStatus;
    
    public String NonFilerPeriods;
    
    public String Office;
    
    public String PAN;
    
    public String PanRegistrationDate;
    
    public String StreetName;
    
    public String Telephone;
    
    public String TradeName;
    
    public String VATAccountStatus;
    
    public String VatRegistrationDate;
    
    public String Ward;

    public PANSearchDetails()
    {
    }

    public PANSearchDetails(Object paramObj, ExtendedSoapSerializationEnvelope __envelope)
    {
	    
	    if (paramObj == null)
            return;
        AttributeContainer inObj=(AttributeContainer)paramObj;


        if(inObj instanceof SoapObject)
        {
            SoapObject soapObject=(SoapObject)inObj;
            int size = soapObject.getPropertyCount();
            for (int i0=0;i0< size;i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info=soapObject.getPropertyInfo(i0);
                Object obj = info.getValue();
                if (info.name.equals("CityName"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.CityName = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.CityName = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("ExciseAccountStatus"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.ExciseAccountStatus = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.ExciseAccountStatus = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("ExciseRegistrationDate"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.ExciseRegistrationDate = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.ExciseRegistrationDate = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("FilingPeriod"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.FilingPeriod = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.FilingPeriod = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("ITAccountStatus"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.ITAccountStatus = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.ITAccountStatus = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("NonFilerPeriods"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.NonFilerPeriods = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.NonFilerPeriods = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("Office"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.Office = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.Office = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("PAN"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.PAN = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.PAN = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("PanRegistrationDate"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.PanRegistrationDate = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.PanRegistrationDate = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("StreetName"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.StreetName = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.StreetName = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("Telephone"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.Telephone = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.Telephone = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("TradeName"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.TradeName = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.TradeName = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("VATAccountStatus"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.VATAccountStatus = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.VATAccountStatus = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("VatRegistrationDate"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.VatRegistrationDate = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.VatRegistrationDate = (String)obj;
                        }
                    }
                    continue;
                }
                if (info.name.equals("Ward"))
                {
                    if(obj!=null)
                    {
        
                        if (obj.getClass().equals(SoapPrimitive.class))
                        {
                            SoapPrimitive j =(SoapPrimitive) obj;
                            if(j.toString()!=null)
                            {
                                this.Ward = j.toString();
                            }
                        }
                        else if (obj instanceof String){
                            this.Ward = (String)obj;
                        }
                    }
                    continue;
                }

            }

        }



    }

    @Override
    public Object getProperty(int propertyIndex) {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if(propertyIndex==0)
        {
            return this.CityName!=null?this.CityName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==1)
        {
            return this.ExciseAccountStatus!=null?this.ExciseAccountStatus:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==2)
        {
            return this.ExciseRegistrationDate!=null?this.ExciseRegistrationDate:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==3)
        {
            return this.FilingPeriod!=null?this.FilingPeriod:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==4)
        {
            return this.ITAccountStatus!=null?this.ITAccountStatus:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==5)
        {
            return this.NonFilerPeriods!=null?this.NonFilerPeriods:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==6)
        {
            return this.Office!=null?this.Office:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==7)
        {
            return this.PAN!=null?this.PAN:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==8)
        {
            return this.PanRegistrationDate!=null?this.PanRegistrationDate:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==9)
        {
            return this.StreetName!=null?this.StreetName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==10)
        {
            return this.Telephone!=null?this.Telephone:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==11)
        {
            return this.TradeName!=null?this.TradeName:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==12)
        {
            return this.VATAccountStatus!=null?this.VATAccountStatus:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==13)
        {
            return this.VatRegistrationDate!=null?this.VatRegistrationDate:SoapPrimitive.NullSkip;
        }
        if(propertyIndex==14)
        {
            return this.Ward!=null?this.Ward:SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount() {
        return 15;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if(propertyIndex==0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "CityName";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "ExciseAccountStatus";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "ExciseRegistrationDate";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "FilingPeriod";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "ITAccountStatus";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "NonFilerPeriods";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==6)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "Office";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==7)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "PAN";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==8)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "PanRegistrationDate";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==9)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "StreetName";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==10)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "Telephone";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==11)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "TradeName";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==12)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "VATAccountStatus";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==13)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "VatRegistrationDate";
            info.namespace= "http://www.pcs.com.np/types/";
        }
        if(propertyIndex==14)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "Ward";
            info.namespace= "http://www.pcs.com.np/types/";
        }
    }
    
    @Override
    public void setProperty(int arg0, Object arg1)
    {
    }

}
