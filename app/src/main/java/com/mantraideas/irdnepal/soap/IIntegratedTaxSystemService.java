package com.mantraideas.irdnepal.soap;




import org.ksoap2.HeaderProperty;
import org.ksoap2.serialization.*;
import org.ksoap2.transport.*;

import java.util.List;


public class IIntegratedTaxSystemService {
    interface HQRIWcfMethod {
        ExtendedSoapSerializationEnvelope CreateSoapEnvelope() throws java.lang.Exception;

        java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, java.lang.Object result) throws java.lang.Exception;
    }

    String url = "http://webservice.ird.gov.np/Services.svc/soap11";

    int timeOut = 60000;
    public List<HeaderProperty> httpHeaders;
    public boolean enableLogging;

    IServiceEvents callback;

    public IIntegratedTaxSystemService() {
    }

    public IIntegratedTaxSystemService(IServiceEvents callback) {
        this.callback = callback;
    }

    public IIntegratedTaxSystemService(IServiceEvents callback, String url) {
        this.callback = callback;
        this.url = url;
    }

    public IIntegratedTaxSystemService(IServiceEvents callback, String url, int timeOut) {
        this.callback = callback;
        this.url = url;
        this.timeOut = timeOut;
    }

    protected org.ksoap2.transport.Transport createTransport() {
        try {
            java.net.URI uri = new java.net.URI(url);
            if (uri.getScheme().equalsIgnoreCase("https")) {
                int port = uri.getPort() > 0 ? uri.getPort() : 443;
                return new HttpsTransportSE(uri.getHost(), port, uri.getPath(), timeOut);
            } else {
                return new HttpTransportSE(url, timeOut);
            }

        } catch (java.net.URISyntaxException e) {
        }
        return null;
    }

    protected ExtendedSoapSerializationEnvelope createEnvelope() {
        ExtendedSoapSerializationEnvelope envelope = new ExtendedSoapSerializationEnvelope(ExtendedSoapSerializationEnvelope.VER11);
        return envelope;
    }

    protected java.util.List sendRequest(String methodName, ExtendedSoapSerializationEnvelope envelope, org.ksoap2.transport.Transport transport) throws java.lang.Exception {
        return transport.call(methodName, envelope, httpHeaders);
    }

    java.lang.Object getResult(java.lang.Class destObj, java.lang.Object source, String resultName, ExtendedSoapSerializationEnvelope __envelope) throws java.lang.Exception {
        if (source == null) {
            return null;
        }
        if (source instanceof SoapPrimitive) {
            SoapPrimitive soap = (SoapPrimitive) source;
            if (soap.getName().equals(resultName)) {
                java.lang.Object instance = __envelope.get(source, destObj);
                return instance;
            }
        } else {
            SoapObject soap = (SoapObject) source;
            if (soap.hasProperty(resultName)) {
                java.lang.Object j = soap.getProperty(resultName);
                if (j == null) {
                    return null;
                }
                java.lang.Object instance = __envelope.get(j, destObj);
                return instance;
            } else if (soap.getName().equals(resultName)) {
                java.lang.Object instance = __envelope.get(source, destObj);
                return instance;
            }
        }

        return null;
    }


    public LoginResponse ValidateOfficerLogin(final String clientId, final String token, final String username, final String password ) throws Exception
    {
        return (LoginResponse)execute(new HQRIWcfMethod()
        {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope(){
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "ValidateOfficerLogin");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info=null;
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="clientId";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(clientId!=null?clientId:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="token";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(token!=null?token:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="username";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(username!=null?username:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="password";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(password!=null?password:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, Object __result)throws Exception {
                return (LoginResponse)getResult(LoginResponse.class,__result,"ValidateOfficerLoginResult",__envelope);
            }
        },"http://tempuri.org/IIntegratedTaxSystemService/ValidateOfficerLogin");
    }

    public android.os.AsyncTask< Void, Void, OperationResult<LoginResponse>> ValidateOfficerLoginAsync(final String clientId, final String token, final String username, final String password)
    {
        return executeAsync(new Functions.IFunc<LoginResponse>() {
            public LoginResponse Func() throws Exception {
                return ValidateOfficerLogin( clientId,token,username,password);
            }
        });
    }

    public TaxpayerResponse ValidatePan(final String pan) throws java.lang.Exception {
        return (TaxpayerResponse) execute(new HQRIWcfMethod() {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope() {
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "ValidatePan");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "pan";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(pan != null ? pan : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception {
                return (TaxpayerResponse) getResult(TaxpayerResponse.class, __result, "ValidatePanResult", __envelope);
            }
        }, "http://tempuri.org/IIntegratedTaxSystemService/ValidatePan");
    }

    public android.os.AsyncTask<Void, Void, OperationResult<TaxpayerResponse>> ValidatePanAsync(final String pan) {
        return executeAsync(new Functions.IFunc<TaxpayerResponse>() {
            public TaxpayerResponse Func() throws java.lang.Exception {
                return ValidatePan(pan);
            }
        });
    }

    public Boolean ValidatePan1(final String pan) throws java.lang.Exception {
        return (Boolean) execute(new HQRIWcfMethod() {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope() {
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "ValidatePan1");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "pan";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(pan != null ? pan : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception {
                SoapObject __soap = (SoapObject) __result;
                java.lang.Object obj = __soap.getProperty("ValidatePan1Result");
                if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                    SoapPrimitive j = (SoapPrimitive) obj;
                    return new Boolean(j.toString());
                } else if (obj != null && obj instanceof Boolean) {
                    return (Boolean) obj;
                }
                return null;
            }
        }, "http://tempuri.org/IIntegratedTaxSystemService/ValidatePan1");
    }

    public android.os.AsyncTask<Void, Void, OperationResult<Boolean>> ValidatePan1Async(final String pan) {
        return executeAsync(new Functions.IFunc<Boolean>() {
            public Boolean Func() throws java.lang.Exception {
                return ValidatePan1(pan);
            }
        });
    }

    public String ValidatePan2(final String pan) throws java.lang.Exception {
        return (String) execute(new HQRIWcfMethod() {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope() {
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "ValidatePan2");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "pan";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(pan != null ? pan : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception {
                SoapObject __soap = (SoapObject) __result;
                java.lang.Object obj = __soap.getProperty("ValidatePan2Result");
                if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                    SoapPrimitive j = (SoapPrimitive) obj;
                    return j.toString();
                } else if (obj != null && obj instanceof String) {
                    return (String) obj;
                }
                return null;
            }
        }, "http://tempuri.org/IIntegratedTaxSystemService/ValidatePan2");
    }

    public android.os.AsyncTask<Void, Void, OperationResult<String>> ValidatePan2Async(final String pan) {
        return executeAsync(new Functions.IFunc<String>() {
            public String Func() throws java.lang.Exception {
                return ValidatePan2(pan);
            }
        });
    }

    public PANSearchResponse SearchPANDetails(final String clientId, final String token, final String pan) throws java.lang.Exception {
        return (PANSearchResponse) execute(new HQRIWcfMethod() {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope() {
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "SearchPANDetails");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "clientId";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(clientId != null ? clientId : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "token";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(token != null ? token : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "pan";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(pan != null ? pan : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception {
                return (PANSearchResponse) getResult(PANSearchResponse.class, __result, "SearchPANDetailsResult", __envelope);
            }
        }, "http://tempuri.org/IIntegratedTaxSystemService/SearchPANDetails");
    }

    public android.os.AsyncTask<Void, Void, OperationResult<PANSearchResponse>> SearchPANDetailsAsync(final String clientId, final String token, final String pan) {
        return executeAsync(new Functions.IFunc<PANSearchResponse>() {
            public PANSearchResponse Func() throws java.lang.Exception {
                return SearchPANDetails(clientId, token, pan);
            }
        });
    }
    public TDSResponse GetWithHoldeeTransactions(final String clientId,final String token,final String pan,final String fromDate,final String toDate ) throws java.lang.Exception
    {
        return (TDSResponse)execute(new HQRIWcfMethod()
        {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope(){
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "GetWithHoldeeTransactions");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info=null;
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="clientId";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(clientId!=null?clientId:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="token";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(token!=null?token:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="pan";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(pan!=null?pan:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="fromDate";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(fromDate!=null?fromDate:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="toDate";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(toDate!=null?toDate:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope,java.lang.Object __result)throws java.lang.Exception {
                return (TDSResponse)getResult(TDSResponse.class,__result,"GetWithHoldeeTransactionsResult",__envelope);
            }
        },"http://tempuri.org/IIntegratedTaxSystemService/GetWithHoldeeTransactions");
    }

    public android.os.AsyncTask< Void, Void, OperationResult< TDSResponse>> GetWithHoldeeTransactionsAsync(final String clientId,final String token,final String pan,final String fromDate,final String toDate)
    {
        return executeAsync(new Functions.IFunc< TDSResponse>() {
            public TDSResponse Func() throws java.lang.Exception {
                return GetWithHoldeeTransactions( clientId,token,pan,fromDate,toDate);
            }
        });
    }

    public SMSResponse GetRegistrationDetailsForMobile(final String clientId, final String token, final String pan) throws java.lang.Exception {
        return (SMSResponse) execute(new HQRIWcfMethod() {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope() {
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "GetRegistrationDetailsForMobile");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "clientId";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(clientId != null ? clientId : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "token";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(token != null ? token : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "pan";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(pan != null ? pan : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception {
                return (SMSResponse) getResult(SMSResponse.class, __result, "GetRegistrationDetailsForMobileResult", __envelope);
            }
        }, "http://tempuri.org/IIntegratedTaxSystemService/GetRegistrationDetailsForMobile");
    }

    public android.os.AsyncTask<Void, Void, OperationResult<SMSResponse>> GetRegistrationDetailsForMobileAsync(final String clientId, final String token, final String pan) {
        return executeAsync(new Functions.IFunc<SMSResponse>() {
            public SMSResponse Func() throws java.lang.Exception {
                return GetRegistrationDetailsForMobile(clientId, token, pan);
            }
        });
    }

    public SMSResponse GetITReturnsInfoForMobile(final String clientId, final String token, final String pan, final String FY) throws java.lang.Exception {
        return (SMSResponse) execute(new HQRIWcfMethod() {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope() {
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "GetITReturnsInfoForMobile");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "clientId";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(clientId != null ? clientId : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "token";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(token != null ? token : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "pan";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(pan != null ? pan : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "FY";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(FY != null ? FY : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception {
                return (SMSResponse) getResult(SMSResponse.class, __result, "GetITReturnsInfoForMobileResult", __envelope);
            }
        }, "http://tempuri.org/IIntegratedTaxSystemService/GetITReturnsInfoForMobile");
    }

    public android.os.AsyncTask<Void, Void, OperationResult<SMSResponse>> GetITReturnsInfoForMobileAsync(final String clientId, final String token, final String pan, final String FY) {
        return executeAsync(new Functions.IFunc<SMSResponse>() {
            public SMSResponse Func() throws java.lang.Exception {
                return GetITReturnsInfoForMobile(clientId, token, pan, FY);
            }
        });
    }

    public SMSResponse GetExciseReturnsInfoForMobile(final String clientId, final String token, final String pan, final String year, final String filingPeriod, final String period) throws java.lang.Exception {
        return (SMSResponse) execute(new HQRIWcfMethod() {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope() {
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "GetExciseReturnsInfoForMobile");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "clientId";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(clientId != null ? clientId : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "token";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(token != null ? token : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "pan";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(pan != null ? pan : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "year";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(year != null ? year : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "filingPeriod";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(filingPeriod != null ? filingPeriod : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "period";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(period != null ? period : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception {
                return (SMSResponse) getResult(SMSResponse.class, __result, "GetExciseReturnsInfoForMobileResult", __envelope);
            }
        }, "http://tempuri.org/IIntegratedTaxSystemService/GetExciseReturnsInfoForMobile");
    }

    public android.os.AsyncTask<Void, Void, OperationResult<SMSResponse>> GetExciseReturnsInfoForMobileAsync(final String clientId, final String token, final String pan, final String year, final String filingPeriod, final String period) {
        return executeAsync(new Functions.IFunc<SMSResponse>() {
            public SMSResponse Func() throws java.lang.Exception {
                return GetExciseReturnsInfoForMobile(clientId, token, pan, year, filingPeriod, period);
            }
        });
    }

    public SMSResponse GetVATReturnsInfoForMobile(final String clientId,final String token,final String pan,final String year,final String filingPeriod,final String period ) throws java.lang.Exception
    {
        return (SMSResponse)execute(new HQRIWcfMethod()
        {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope(){
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "GetVATReturnsInfoForMobile");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info=null;
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="clientId";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(clientId!=null?clientId:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="token";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(token!=null?token:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="pan";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(pan!=null?pan:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="year";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(year!=null?year:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="filingPeriod";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(filingPeriod!=null?filingPeriod:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="period";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(period!=null?period:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope,java.lang.Object __result)throws java.lang.Exception {
                return (SMSResponse)getResult(SMSResponse.class,__result,"GetVATReturnsInfoForMobileResult",__envelope);
            }
        },"http://tempuri.org/IIntegratedTaxSystemService/GetVATReturnsInfoForMobile");
    }

    public android.os.AsyncTask< Void, Void, OperationResult< SMSResponse>> GetVATReturnsInfoForMobileAsync(final String clientId,final String token,final String pan,final String year,final String filingPeriod,final String period)
    {
        return executeAsync(new Functions.IFunc< SMSResponse>() {
            public SMSResponse Func() throws java.lang.Exception {
                return GetVATReturnsInfoForMobile( clientId,token,pan,year,filingPeriod,period);
            }
        });
    }


    public SMSResponse GetEstimatedReturnsInfoForMobile(final String clientId, final String token, final String pan, final String FY) throws java.lang.Exception {
        return (SMSResponse) execute(new HQRIWcfMethod() {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope() {
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "GetEstimatedReturnsInfoForMobile");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "clientId";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(clientId != null ? clientId : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "token";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(token != null ? token : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "pan";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(pan != null ? pan : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "FY";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(FY != null ? FY : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception {
                return (SMSResponse) getResult(SMSResponse.class, __result, "GetEstimatedReturnsInfoForMobileResult", __envelope);
            }
        }, "http://tempuri.org/IIntegratedTaxSystemService/GetEstimatedReturnsInfoForMobile");
    }

    public android.os.AsyncTask<Void, Void, OperationResult<SMSResponse>> GetEstimatedReturnsInfoForMobileAsync(final String clientId, final String token, final String pan, final String FY) {
        return executeAsync(new Functions.IFunc<SMSResponse>() {
            public SMSResponse Func() throws java.lang.Exception {
                return GetEstimatedReturnsInfoForMobile(clientId, token, pan, FY);
            }
        });
    }




    public SMSResponse GetPaymentInfoForMobile(final String clientId,final String token,final String pan,final String Voucher ) throws java.lang.Exception
    {
        return (SMSResponse)execute(new HQRIWcfMethod()
        {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope(){
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "GetPaymentInfoForMobile");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info=null;
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="clientId";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(clientId!=null?clientId:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="token";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(token!=null?token:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="pan";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(pan!=null?pan:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="Voucher";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(Voucher!=null?Voucher:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope,java.lang.Object __result)throws java.lang.Exception {
                return (SMSResponse)getResult(SMSResponse.class,__result,"GetPaymentInfoForMobileResult",__envelope);
            }
        },"http://tempuri.org/IIntegratedTaxSystemService/GetPaymentInfoForMobile");
    }
    public android.os.AsyncTask< Void, Void, OperationResult< SMSResponse>> GetPaymentInfoForMobileAsync(final String clientId,final String token,final String pan,final String Voucher)
    {
        return executeAsync(new Functions.IFunc< SMSResponse>() {
            public SMSResponse Func() throws java.lang.Exception {
                return GetPaymentInfoForMobile( clientId,token,pan,Voucher);
            }
        });
    }

    public SMSResponse GetArrearForMobile(final String clientId,final String token,final String pan ) throws java.lang.Exception
    {
        return (SMSResponse)execute(new HQRIWcfMethod()
        {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope(){
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "GetArrearForMobile");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info=null;
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="clientId";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(clientId!=null?clientId:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="token";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(token!=null?token:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="pan";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(pan!=null?pan:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope,java.lang.Object __result)throws java.lang.Exception {
                return (SMSResponse)getResult(SMSResponse.class,__result,"GetArrearForMobileResult",__envelope);
            }
        },"http://tempuri.org/IIntegratedTaxSystemService/GetArrearForMobile");
    }

    public android.os.AsyncTask< Void, Void, OperationResult< SMSResponse>> GetArrearForMobileAsync(final String clientId,final String token,final String pan)
    {
        return executeAsync(new Functions.IFunc< SMSResponse>() {
            public SMSResponse Func() throws java.lang.Exception {
                return GetArrearForMobile( clientId,token,pan);
            }
        });
    }


    public SMSResponse GetCollectionForMobile(final String clientId,final String token,final String FromDate,final String ToDate ) throws java.lang.Exception
    {
        return (SMSResponse)execute(new HQRIWcfMethod()
        {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope(){
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "GetCollectionForMobile");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info=null;
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="clientId";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(clientId!=null?clientId:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="token";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(token!=null?token:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="FromDate";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(FromDate!=null?FromDate:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="http://tempuri.org/";
                __info.name="ToDate";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(ToDate!=null?ToDate:SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope,java.lang.Object __result)throws java.lang.Exception {
                return (SMSResponse)getResult(SMSResponse.class,__result,"GetCollectionForMobileResult",__envelope);
            }
        },"http://tempuri.org/IIntegratedTaxSystemService/GetCollectionForMobile");
    }

    public android.os.AsyncTask< Void, Void, OperationResult< SMSResponse>> GetCollectionForMobileAsync(final String clientId,final String token,final String FromDate,final String ToDate )
    {
        return executeAsync(new Functions.IFunc< SMSResponse>() {
            public SMSResponse Func() throws java.lang.Exception {
                return GetCollectionForMobile( clientId,token,FromDate,ToDate);
            }
        });
    }



    public TaxpayerLoginResponse ValidateTaxpayerLogin(final String clientId, final String token, final String pan, final String username, final String password) throws Exception {
        return (TaxpayerLoginResponse) execute(new HQRIWcfMethod() {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope() {
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "ValidateTaxpayerLogin");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "clientId";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(clientId != null ? clientId : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "token";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(token != null ? token : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "pan";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(pan != null ? pan : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "username";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(username != null ? username : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "password";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(password != null ? password : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, Object __result) throws Exception {
                return (TaxpayerLoginResponse) getResult(TaxpayerLoginResponse.class, __result, "ValidateTaxpayerLoginResult", __envelope);
            }
        }, "http://tempuri.org/IIntegratedTaxSystemService/ValidateTaxpayerLogin");
    }

    public android.os.AsyncTask<Void, Void, OperationResult<TaxpayerLoginResponse>> ValidateTaxpayerLoginAsync(final String clientId, final String token, final String pan, final String username, final String password) {
        return executeAsync(new Functions.IFunc<TaxpayerLoginResponse>() {
            public TaxpayerLoginResponse Func() throws Exception {
                return ValidateTaxpayerLogin(clientId, token, pan, username, password);
            }
        });
    }
    public TaxpayerLoginResponse ValidateTDSTaxpayerLogin(final String clientId, final String token, final String pan, final String username, final String password) throws Exception {
        return (TaxpayerLoginResponse) execute(new HQRIWcfMethod() {
            @Override
            public ExtendedSoapSerializationEnvelope CreateSoapEnvelope() {
                ExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://tempuri.org/", "ValidateTDSTaxpayerLogin");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "clientId";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(clientId != null ? clientId : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "token";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(token != null ? token : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "pan";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(pan != null ? pan : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "username";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(username != null ? username : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://tempuri.org/";
                __info.name = "password";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(password != null ? password : SoapPrimitive.NullSkip);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public Object ProcessResult(ExtendedSoapSerializationEnvelope __envelope, Object __result) throws Exception {
                return (TaxpayerLoginResponse) getResult(TaxpayerLoginResponse.class, __result, "ValidateTDSTaxpayerLoginResult", __envelope);
            }
        }, "http://tempuri.org/IIntegratedTaxSystemService/ValidateTDSTaxpayerLogin");
    }

    public android.os.AsyncTask<Void, Void, OperationResult<TaxpayerLoginResponse>> ValidateTDSTaxpayerLoginAsync(final String clientId, final String token, final String pan, final String username, final String password) {
        return executeAsync(new Functions.IFunc<TaxpayerLoginResponse>() {
            public TaxpayerLoginResponse Func() throws Exception {
                return ValidateTDSTaxpayerLogin(clientId, token, pan, username, password);
            }
        });
    }

    protected java.lang.Object execute(HQRIWcfMethod wcfMethod, String methodName) throws java.lang.Exception {
        org.ksoap2.transport.Transport __httpTransport = createTransport();
        __httpTransport.debug = enableLogging;
        ExtendedSoapSerializationEnvelope __envelope = wcfMethod.CreateSoapEnvelope();
        try {
            sendRequest(methodName, __envelope, __httpTransport);

        } finally {
            if (__httpTransport.debug) {
                if (__httpTransport.requestDump != null) {
                    android.util.Log.i("requestDump", __httpTransport.requestDump);

                }
                if (__httpTransport.responseDump != null) {
                    android.util.Log.i("responseDump", __httpTransport.responseDump);
                }
            }
        }
        java.lang.Object __retObj = __envelope.bodyIn;
        if (__retObj instanceof org.ksoap2.SoapFault) {
            org.ksoap2.SoapFault __fault = (org.ksoap2.SoapFault) __retObj;
            throw convertToException(__fault, __envelope);
        } else {
            return wcfMethod.ProcessResult(__envelope, __retObj);
        }
    }

    protected <T> android.os.AsyncTask<Void, Void, OperationResult<T>> executeAsync(final Functions.IFunc<T> func) {
        return new android.os.AsyncTask<Void, Void, OperationResult<T>>() {
            @Override
            protected void onPreExecute() {
                callback.Starting();
            };

            @Override
            protected OperationResult<T> doInBackground(Void... params) {
                OperationResult<T> result = new OperationResult<T>();
                try {
                    result.Result = func.Func();
                } catch (java.lang.Exception ex) {
                    ex.printStackTrace();
                    result.Exception = ex;
                }
                return result;
            }

            @Override
            protected void onPostExecute(OperationResult<T> result) {
                callback.Completed(result);
            }
        }.execute();
    }

    java.lang.Exception convertToException(org.ksoap2.SoapFault fault, ExtendedSoapSerializationEnvelope envelope) {

        return new java.lang.Exception(fault.faultstring);
    }
}

