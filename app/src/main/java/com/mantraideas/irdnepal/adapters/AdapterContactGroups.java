package com.mantraideas.irdnepal.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.activities.ContactDetailActivity;
import com.mantraideas.irdnepal.activities.MapsActivity;
import com.mantraideas.irdnepal.models.Generic;
import com.mantraideas.irdnepal.utilities.DBConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by rowsun on 5/27/16.
 */
public class AdapterContactGroups extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_LIST = 1;
    Context context;
    LayoutInflater layoutInflater;
    List<Generic> genericList;

    public AdapterContactGroups(Context context, List<Generic> genericList) {
        this.genericList = genericList;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_LIST) {
            View v = layoutInflater.inflate(R.layout.row_contact_list, parent, false);
            return new ViewHolderContactGroups(v);
        } else if(viewType == VIEW_TYPE_HEADER){
            View v = layoutInflater.inflate(R.layout.row_header_contact_list, parent, false);
            return new ViewHolderHeader(v);
        }
        return null;

    }

    @Override
    public int getItemViewType(int position) {
        return genericList.get(position).type == VIEW_TYPE_HEADER ? VIEW_TYPE_HEADER : VIEW_TYPE_LIST;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            if(holder instanceof ViewHolderHeader){
                    ViewHolderHeader mHolder = (ViewHolderHeader) holder;
                    mHolder.header.setText(genericList.get(position).data);
                }else if (holder instanceof ViewHolderContactGroups) {
                    ViewHolderContactGroups mHolder = (ViewHolderContactGroups) holder;
                    try {
                        JSONObject object = new JSONObject(genericList.get(position).data);
                        mHolder.ofc_name.setText(object.optString(DBConstants.COLUMN_NAME));
                        mHolder.address.setText(object.optString(DBConstants.COLUMN_ADDRESS));
                        mHolder.card_contact_list.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent i = new Intent(context, ContactDetailActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.putExtra("data",genericList.get(position).data);
                                context.startActivity(i);;
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }

    }

    @Override
    public int getItemCount() {
        return genericList.size();
    }

    class ViewHolderContactGroups extends RecyclerView.ViewHolder {
        TextView ofc_name, address;
        CardView card_contact_list;
        public ViewHolderContactGroups(View itemView) {
            super(itemView);
            ofc_name = (TextView) itemView.findViewById(R.id.ofc_name);
            address = (TextView) itemView.findViewById(R.id.address);
            card_contact_list = (CardView) itemView.findViewById(R.id.card_contact_list);
        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        TextView header;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            header = (TextView) itemView.findViewById(R.id.header);
        }
    }
}
