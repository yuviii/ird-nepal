package com.mantraideas.irdnepal.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.MyDownloader;
import com.mantraideas.irdnepal.helpers.Preference;
import com.mantraideas.irdnepal.models.Attachments;
import com.mantraideas.irdnepal.utilities.Utils;

import java.util.List;

/**
 * Created by rowsun on 5/25/16.
 */
public class AdapterAttachment extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<Attachments> attachmentsList;
    LayoutInflater layoutInflater;
    Preference pf;
    String lang;

    public AdapterAttachment(Context context, List<Attachments> attachmentsList) {
        this.context = context;
        this.attachmentsList = attachmentsList;
        layoutInflater = LayoutInflater.from(context);
        pf = new Preference(this.context);
        lang = pf.getPreference("pref_lang");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.row_document, parent, false);
        return new viewHolderNotice(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof viewHolderNotice) {
            final viewHolderNotice mHolder = (viewHolderNotice) holder;
            Attachments a = attachmentsList.get(position);
            if(a.LanguageInAttachMent.equals("1")){
                mHolder.heading.setText(a.Comment);
            }else if(a.LanguageInAttachMent.equals("2")){
                mHolder.heading.setText(a.Comment2);
            }else {
                mHolder.heading.setText(a.Comment);
            }
            mHolder.download.setImageResource((Utils.checkFileExist(attachmentsList.get(position).AttachmentUrl) ? R.drawable.eye : R.drawable.downoad));
            mHolder.date.setText(Utils.getRelativeTime(Long.parseLong(attachmentsList.get(position).LastUpdateDate) * 1000));
            mHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.getUrl(attachmentsList.get(position).AttachmentUrl);
                    if (Utils.checkFileExist(attachmentsList.get(position).AttachmentUrl)) {
                        Utils.log(Utils.getSDPathFromUrl(attachmentsList.get(position).AttachmentUrl).toString());
                        MyDownloader.readPdfFile(context, Utils.getSDPathFromUrl(attachmentsList.get(position).AttachmentUrl));
                    } else if (!Utils.isNetworkConnected(context)) {
                        Utils.snack(mHolder.itemView.getRootView(), "No internet connection");

                    } else {
                        String url = Utils.getUrl(attachmentsList.get(position).AttachmentUrl);
                        if (url != null) {
                            Utils.log(url);
                            new MyDownloader(context, position, url, new MyDownloader.onDownloadCompleteListner() {
                                @Override
                                public void downloadListener(int index, String link, boolean completed) {
                                    if (completed) {
                                        mHolder.download.setImageResource(R.drawable.eye);
                                        notifyDataSetChanged();
                                    }
                                }
                            }).execute();
                        } else {
                            Utils.snack(v, "Invalid URL");
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return attachmentsList.size();
    }

    class viewHolderNotice extends RecyclerView.ViewHolder {
        TextView heading, date;
        ImageView download;
        public viewHolderNotice(View itemView) {
            super(itemView);
            heading = (TextView) itemView.findViewById(R.id.docName);
            download = (ImageView) itemView.findViewById(R.id.downloadDoc);
            date = (TextView) itemView.findViewById(R.id.date);
        }
    }
}
