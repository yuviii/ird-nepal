package com.mantraideas.irdnepal.adapters;

import android.content.Context;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mantraideas.irdnepal.R;
import com.mantraideas.irdnepal.helpers.MyDownloader;
import com.mantraideas.irdnepal.models.Notice;
import com.mantraideas.irdnepal.utilities.Utils;

import java.util.List;

/**
 * Created by rowsun on 5/23/16.
 */
public class AdapterNoticeView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<Notice> noticeList;
    LayoutInflater layoutInflater;
    public AdapterNoticeView(Context context, List<Notice> noticeList) {
        this.context = context;
        this.noticeList = noticeList;
        layoutInflater=LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.row_notice, parent, false);
        return new viewHolderNotice(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof viewHolderNotice) {
            final viewHolderNotice mHolder = (viewHolderNotice) holder;
            mHolder.heading.setText(noticeList.get(position).Heading);
            mHolder.date.setText(Utils.getRelativeTime(Long.parseLong(noticeList.get(position).DatedOn)*1000));
            mHolder.download.setImageResource((noticeList.get(position).isDownloaded ? R.drawable.eye : R.drawable.downoad));
            mHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (noticeList.get(position).isDownloaded) {
                        Utils.log(Utils.getSDPathFromUrl(noticeList.get(position).Url).toString());
                        MyDownloader.readPdfFile(context, Utils.getSDPathFromUrl(noticeList.get(position).Url));
                    } else if (!Utils.isNetworkConnected(context)) {
                        Utils.snack(mHolder.itemView.getRootView(), "No internet connection");
                    } else {
                        String url = Utils.getUrl(noticeList.get(position).Url);
                        Utils.log(url);
                        if (url != null) {
                            new MyDownloader(v.getContext(), position, url, new MyDownloader.onDownloadCompleteListner() {
                                @Override
                                public void downloadListener(int index, String link, boolean completed) {
                                    if (completed) {
                                        noticeList.get(index).isDownloaded = true;
                                        mHolder.download.setImageResource(R.drawable.eye);
                                        notifyDataSetChanged();
                                    }
                                }
                            }).execute();
                        }else {
                            Utils.snack(v,"Invalid URL");
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return noticeList.size();
    }

    class viewHolderNotice extends RecyclerView.ViewHolder {
        TextView heading,date;
        ImageView download;
        public viewHolderNotice(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            heading = (TextView) itemView.findViewById(R.id.notice);
            download = (ImageView) itemView.findViewById(R.id.downloadNotice);
        }
    }
}
