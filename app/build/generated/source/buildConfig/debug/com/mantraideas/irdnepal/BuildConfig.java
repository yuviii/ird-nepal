/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mantraideas.irdnepal;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.mantraideas.irdnepal";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 16;
  public static final String VERSION_NAME = "0.0.16";
}
