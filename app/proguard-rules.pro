# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/mantraideas/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep class com.google.android.** { *; }
-dontwarn com.google.android.**

# for v4 and v7
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep class android.support.v7.app.** { *; }
-keep interface android.support.v7.app.** { *; }
-keep class android.support.v7.widget.** { *; }
-keep class android.support.v4.widget.** { *; }
-keep class android.support.design.widget.** { *; }
-keep class android.support.**{*;}
-dontwarn android.support.**
# for parse
-keep class com.parse.** { *; }
-dontwarn com.parse.**

-keep class com.google.firebase.** { *; }
-dontwarn com.google.**

# picasso
-keep class com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**

# Keep all the ACRA classes
-keep class org.acra.** { *; }
-dontwarn org.acra.**
-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**
-keep class okhttp3.**{*;}
-keep interface okhttp3.**{*;}
-dontwarn okhttp3.**
-keep class okio.**{*;}
-keep interface okio.**{*;}
-dontwarn okio.**
-keep class org.**{*;}
-keep interface org.**{*;}
-dontwarn org.**
-keepattributes Signature
-keepattributes *Annotation*
-keep class com.squareup.okhttp3.** { *; }
-keep interface com.squareup.okhttp3.** { *; }
-dontwarn com.squareup.okhttp3.**

#facebook
-keep class com.facebook.** {*;}
-dontwarn com.facebook.**
-keepattributes Signature
-keep class com.backendless.** {*;}
-dontwarn com.backendless.**
-keep class com.fasterxml.jackson.** {*;}
-dontwarn com.fasterxml.jackson.**
-keep class org.springframework.** {*;}
-dontwarn org.springframework.**
-keep class weborb.**{*;}
-dontwarn weborb.**